from .common import *             # NOQA
import sys
import logging.config
from os.path import exists


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

# Use 12factor inspired environment variables or from a file
import environ
env = environ.Env()
env_file = join(BASE_DIR, 'envs/dev.env')
if exists(env_file):
    environ.Env.read_env(str(env_file))

#
#
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env('DB_NAME'),
        'USER': env('DB_USER'),
        'PASSWORD': env('DB_PWD'),
        'HOST': env('DB_HOST'),  # Or an IP Address that your DB is hosted on
        'PORT': env('DB_PORT'),
    }
}
API_HOSTNAME = env('API_HOSTNAME')

SECRET_KEY = env('SECRET_KEY')

API_ENDPOINT = env('API_ENDPOINT')




# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
TEMPLATES[0]['OPTIONS'].update({'debug': True})

# Turn off debug while imported by Celery with a workaround
# See http://stackoverflow.com/a/4806384
if "celery" in sys.argv[0]:
    DEBUG = False

# Django Debug Toolbar
# INSTALLED_APPS += (
#     'debug_toolbar.apps.DebugToolbarConfig',)

# Show emails to console in DEBUG mode
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# Show thumbnail generation errors
THUMBNAIL_DEBUG = True

# Log everything to the logs directory at the top

# Reset logging
# (see http://www.caktusgroup.com/blog/2015/01/27/Django-Logging-Configuration-logging_config-default-settings-logger/)



EMAIL_HOST = env('EMAIL_HOST')
EMAIL_PORT = env('EMAIL_PORT')
EMAIL_HOST_USER = env('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
#EMAIL_USE_TLS = env('EMAIL_USE_TLS')
#DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL')
#CONTACT_TO_EMAIL = env('CONTACT_TO_EMAIL')
