import logging.config
import sys

from .common import *             # NOQA

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

# Use 12factor inspired environment variables or from a file
import environ
env = environ.Env()

env_file = join(BASE_DIR, 'envs/staging.env')
if exists(env_file):
    environ.Env.read_env(str(env_file))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': env('DB_NAME'),
        'USER': env('DB_USER'),
        'PASSWORD': env('DB_PWD'),
        'HOST': env('DB_HOST'),   # Or an IP Address that your DB is hosted on
        'PORT': env('DB_PORT'),
    }
}
API_HOSTNAME = env('API_HOSTNAME')
SECRET_KEY = env('SECRET_KEY')

API_ENDPOINT = env('API_ENDPOINT')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
TEMPLATES[0]['OPTIONS'].update({'debug': False})

# Turn off debug while imported by Celery with a workaround
# See http://stackoverflow.com/a/4806384
if "celery" in sys.argv[0]:
    DEBUG = False
# Must mention ALLOWED_HOSTS in production!
ALLOWED_HOSTS = ["*"]
# Show emails to console in DEBUG mode
+EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# Show thumbnail generation errors
THUMBNAIL_DEBUG = False

# Log everything to the logs directory at the top
LOGFILE_ROOT = join(BASE_DIR, 'logs')
STATICFILES_LOCATION = MEDIAFILES_LOCATION = BASE_DIR

INSTALLED_APPS += ('storages',)
AWS_STORAGE_BUCKET_NAME = "affectlab"
STATICFILES_STORAGE = 'custom_storages.StaticStorage'
S3_STATIC_URL = 'http://%s.s3.amazonaws.com/%s/' % (AWS_STORAGE_BUCKET_NAME, STATICFILES_LOCATION)
STATIC_URL = S3_STATIC_URL


S3_MEDIA_URL = 'http://%s.s3.amazonaws.com/%s/' % (AWS_STORAGE_BUCKET_NAME, MEDIAFILES_LOCATION)
MEDIA_URL= S3_MEDIA_URL
DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'



# Reset logging
# (see http://www.caktusgroup.com/blog/2015/01/27/Django-Logging-Configuration-logging_config-default-settings-logger/)

LOGGING_CONFIG = None
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(pathname)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'django_log_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': join(LOGFILE_ROOT, 'django.log'),
            'formatter': 'verbose'
        },
        'proj_log_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': join(LOGFILE_ROOT, 'project.log'),
            'formatter': 'verbose'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        }
    },
    'loggers': {
        'django': {
            'handlers': ['django_log_file'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'project': {
            'handlers': ['proj_log_file'],
            'level': 'DEBUG',
        },
    }
}

logging.config.dictConfig(LOGGING)

EMAIL_HOST = env('EMAIL_HOST')
EMAIL_PORT = env('EMAIL_PORT')
EMAIL_HOST_USER = env('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = env('EMAIL_USE_TLS')
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL')
CONTACT_TO_EMAIL = env('CONTACT_TO_EMAIL')
