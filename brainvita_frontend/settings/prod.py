# In production set the environment variable like this:
#    DJANGO_SETTINGS_MODULE=chromo.settings.production
from .common import *             # NOQA
import logging.config


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

# Use 12factor inspired environment variables or from a file
import environ
env = environ.Env()

env_file = join(BASE_DIR, 'envs/prod.env')
if exists(env_file):
    environ.Env.read_env(str(env_file))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': env('DB_NAME'),
        'USER': env('DB_USER'),
        'PASSWORD': env('DB_PWD'),
        'HOST': env('DB_HOST'),   # Or an IP Address that your DB is hosted on
        'PORT': env('DB_PORT'),
    }
}
API_HOSTNAME = env('API_HOSTNAME')
SECRET_KEY = env('SECRET_KEY')

API_ENDPOINT = env('API_ENDPOINT')

+EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# For security and performance reasons, DEBUG is turned off
DEBUG = False
TEMPLATE_DEBUG = False

# Must mention ALLOWED_HOSTS in production!
ALLOWED_HOSTS = ["*"]

# Cache the templates in memory for speed-up
loaders = [
    ('django.template.loaders.cached.Loader', [
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    ]),
]

TEMPLATES[0]['OPTIONS'].update({"loaders": loaders})
TEMPLATES[0].update({"APP_DIRS": False})

# Define STATIC_ROOT for the collectstatic command
STATIC_ROOT = join(BASE_DIR, '..', 'site', 'static')

# Log everything to the logs directory at the top
LOGFILE_ROOT = join(dirname(BASE_DIR), 'logs')

INSTALLED_APPS += ('storages',)
AWS_STORAGE_BUCKET_NAME = "chromowebbucket"
STATICFILES_STORAGE = 'custom_storages.StaticStorage'
S3_STATIC_URL = 'http://%s.s3.amazonaws.com/%s/' % (AWS_STORAGE_BUCKET_NAME, STATICFILES_LOCATION)
STATIC_URL = S3_STATIC_URL


S3_MEDIA_URL = 'http://%s.s3.amazonaws.com/%s/' % (AWS_STORAGE_BUCKET_NAME, MEDIAFILES_LOCATION)
MEDIA_URL= S3_MEDIA_URL
DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'

# Reset logging
LOGGING_CONFIG = None
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(pathname)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'proj_log_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': join(LOGFILE_ROOT, 'project.log'),
            'formatter': 'verbose'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        }
    },
    'loggers': {
        'project': {
            'handlers': ['proj_log_file'],
            'level': 'DEBUG',
        },
    }
}

logging.config.dictConfig(LOGGING)

EMAIL_HOST = env('EMAIL_HOST')
EMAIL_PORT = env('EMAIL_PORT')
EMAIL_HOST_USER = env('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = env('EMAIL_USE_TLS')
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL')
CONTACT_TO_EMAIL = env('CONTACT_TO_EMAIL')