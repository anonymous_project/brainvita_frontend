#!/bin/bash
# Starts the Gunicorn server
set -e

# Activate the virtualenv for this project
%(ACTIVATE)s

# Start gunicorn going
exec gunicorn affect_lab_front.wsgi:application --pythonpath . -c %(PROJECT_PATH)s/gunicorn.conf.py



