"""
--------------------------------------------------------------------------------------
tasks.py
--------------------------------------------------------------------------------------
A set of tasks to manage AWS Django deployment.

Tasks include:
    - configure_instance  : Configures a new EC2 instance (as definied in project_conf.py) and return's it's public dns
                            This takes around 8 minutes to complete.

    - update_packages : Updates the python packages on the server to match those found in requirements/common.txt and
                        requirements/prod.txt

    - deploy : Pulls the latest commit from the master branch on the server, collects the static files, syncs the db and
               restarts the server

    - reload_gunicorn : Pushes the gunicorn startup script to the servers and restarts the gunicorn process, use this if you
                        have made changes to templates/start_gunicorn.bash

    - reload_nginx : Pushes the nginx config files to the servers and restarts the nginx, use this if you
                     have made changes to templates/nginx-app-proxy or templates/nginx.conf

    - reload_supervisor : Pushes the supervisor config files to the servers and restarts the supervisor, use this if you
                          have made changes to templates/supervisord-init or templates/supervisord.conf

"""

# Spawns a new EC2 instance (as definied in djangofab_conf.py) and return's it's public dns
# This takes around 8 minutes to complete.
configure_instance = [


  # Set environment variables
  # TODO : Make env files remote
  {"action": "sudo", "params": "mkdir -p %(PROJECT_PATH)s/envs", "message": "Creating directory for env files"},
  {"action": "put", "params": {"file": "%(FAB_CONFIG_PATH)s/envs/%(ENVIRONMENT)s.env",
                               "destination": "/home/%(SERVER_USERNAME)s/%(ENVIRONMENT)s.env"}},
  {"action": "sudo", "params": "\cp /home/%(SERVER_USERNAME)s/%(ENVIRONMENT)s.env %(PROJECT_PATH)s/envs/%(ENVIRONMENT)s.env"},
  {"action": "sudo", "params": "rm -f /home/%(SERVER_USERNAME)s/%(ENVIRONMENT)s.env"},
  {"action": "run", "params": "echo 'export DJANGO_SETTINGS_MODULE=brainvita_frontend.settings' >> /home/%(SERVER_USERNAME)s/.profile"},
  {"action": "run", "params": "source /home/%(SERVER_USERNAME)s/.profile"},


  # Run collectstatic and migrate
  {"action":"virtualenv", "params":"python %(PROJECT_ROOT)s/manage.py collectstatic -v 0 --noinput"},
  {"action":"virtualenv", "params": "%(RUN_MIGRATIONS_CMD)s"},


  # Setup supervisor

  # stop old supervisor process
  {"action": "sudo", "params": "supervisorctl stop all"},
  {"action": "sudo", "params": "killall supervisord"},

  {"action":"run", "params":"echo_supervisord_conf > /home/%(SERVER_USERNAME)s/supervisord.conf",
   "message":"Configuring supervisor"},
  {"action":"put_template", "params":{"template":"%(FAB_CONFIG_PATH)s/templates/supervisord.conf",
                                      "destination":"/home/%(SERVER_USERNAME)s/my.supervisord.conf"}},
  {"action":"run", "params":"cat /home/%(SERVER_USERNAME)s/my.supervisord.conf >> /home/%(SERVER_USERNAME)s/supervisord.conf"},
  {"action":"run", "params":"rm /home/%(SERVER_USERNAME)s/my.supervisord.conf"},
  {"action":"sudo", "params":"mv /home/%(SERVER_USERNAME)s/supervisord.conf /etc/supervisord.conf"},
  {"action":"sudo", "params":"supervisord"},
  {"action":"put", "params":{"file":"%(FAB_CONFIG_PATH)s/templates/supervisord-init",
                             "destination":"/home/%(SERVER_USERNAME)s/supervisord-init"}},
  {"action":"sudo", "params":"mv /home/%(SERVER_USERNAME)s/supervisord-init /etc/init.d/supervisord"},
  {"action":"sudo", "params":"chmod +x /etc/init.d/supervisord"},
  {"action":"sudo", "params":"update-rc.d supervisord defaults"}
]

# Updates environment variables on the deployment environment

update_vars = [
  # Updates the environment variables
  {"action": "sudo", "params": "mkdir -p %(PROJECT_PATH)s/envs", "message": "Creating directory for env files"},
  {"action": "put", "params": {"file": "%(FAB_CONFIG_PATH)s/envs/%(ENVIRONMENT)s.env",
                               "destination": "/home/%(SERVER_USERNAME)s/%(ENVIRONMENT)s.env"}},
  {"action": "sudo", "params": "\cp /home/%(SERVER_USERNAME)s/%(ENVIRONMENT)s.env %(PROJECT_PATH)s/envs/%(ENVIRONMENT)s.env"},
  {"action": "sudo", "params": "rm -f /home/%(SERVER_USERNAME)s/%(ENVIRONMENT)s.env"},
]

# Switches to development branch on environment

switch_dev = [
  # Updates the environment variables
  {"action": "run", "params": "cd %(PROJECT_PATH)s", "message": "Switching to project directory"},
  {"action": "run", "params": "git fetch && git checkout development", "message": "Switching to development branch"},
  {"action": "run", "params": "cd", "message": "Switching to development branch"},
]

# Updates the python packages on the server to match those found in requirements/common.txt and
# requirements/prod.txt
update_packages = [

  # Updates the python packages
  {"action":"virtualenv", "params":"pip install -r %(PROJECT_PATH)s/requirements/common.txt --upgrade"},
  {"action":"virtualenv", "params":"pip install -r %(PROJECT_PATH)s/requirements/prod.txt --upgrade"},
]

# Pulls the latest commit from the master branch on the server, collects the static files, syncs
# the db and restarts the server
deploy = [

  {"action":"put", "params":{"file":"%(FAB_CONFIG_PATH)s/templates/nginx.conf",
                             "destination":"/home/%(SERVER_USERNAME)s/nginx.conf"},
   "message":"Configuring nginx"},
  {"action":"sudo", "params":"mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.old"},
  {"action":"sudo", "params":"mv /home/%(SERVER_USERNAME)s/nginx.conf /etc/nginx/nginx.conf"},
  {"action":"sudo", "params":"chown root:root /etc/nginx/nginx.conf"},
  {"action":"put_template", "params":{"template":"%(FAB_CONFIG_PATH)s/templates/nginx-app-proxy",
                                      "destination":"/home/%(SERVER_USERNAME)s/%(PROJECT_NAME)s/config"}},
  {"action":"sudo", "params":"rm -rf /etc/nginx/sites-enabled/default"},
  {"action":"sudo", "params":"mv /home/%(SERVER_USERNAME)s/%(PROJECT_NAME)s/config /etc/nginx/sites-available/%(PROJECT_NAME)s"},
  {"action":"sudo", "params":"ln -sf /etc/nginx/sites-available/%(PROJECT_NAME)s /etc/nginx/sites-enabled/%(PROJECT_NAME)s"},
  {"action":"sudo", "params":"chown root:root /etc/nginx/sites-available/%(PROJECT_NAME)s"},
  {"action":"sudo", "params":"/etc/init.d/nginx restart", "message":"Restarting nginx"},

]

devdeploy = [

 # {"action": "run", "params": "cd %(PROJECT_PATH)s", "message": "Switching to project directory"},
  #{"action": "run", "params": "ls -a", "message": " print dir"},

 # {"action": "run", "params": "git checkout development", "message": "Switching to development branch"},
  #{"action": "run", "params": "cd", "message": "Switching to development branch"},
  # Pull the latest version from the bitbucket repo
  #{"action":"run", "params":"cd %(PROJECT_PATH)s  && git pull origin master"},

  # Update the database
  #{"action":"virtualenv", "params":"python %(PROJECT_ROOT)s/manage.py collectstatic -v 0 --noinput"},
  #{"action":"virtualenv", "params":"python %(PROJECT_ROOT)s/manage.py makemigrations %(APP_LIST)s"},
  #{"action":"virtualenv", "params":"python %(PROJECT_ROOT)s/manage.py migrate"},

  {"action":"run", "params":"echo_supervisord_conf > /home/%(SERVER_USERNAME)s/supervisord.conf",
   "message":"Configuring supervisor"},
  {"action":"put_template", "params":{"template":"%(FAB_CONFIG_PATH)s/templates/supervisord.conf",
                                      "destination":"/home/%(SERVER_USERNAME)s/my.supervisord.conf"}},
  {"action":"run", "params":"cat /home/%(SERVER_USERNAME)s/my.supervisord.conf >> /home/%(SERVER_USERNAME)s/supervisord.conf"},
  {"action":"run", "params":"rm /home/%(SERVER_USERNAME)s/my.supervisord.conf"},
  {"action":"sudo", "params":"mv /home/%(SERVER_USERNAME)s/supervisord.conf /etc/supervisord.conf"},
  {"action":"sudo", "params":"supervisord"},
  {"action":"put", "params":{"file":"%(FAB_CONFIG_PATH)s/templates/supervisord-init",
                             "destination":"/home/%(SERVER_USERNAME)s/supervisord-init"}},
  {"action":"sudo", "params":"mv /home/%(SERVER_USERNAME)s/supervisord-init /etc/init.d/supervisord"},
  {"action":"sudo", "params":"chmod +x /etc/init.d/supervisord"},
  {"action":"sudo", "params":"update-rc.d supervisord defaults"}
]
# Pushes the gunicorn startup script to the servers and restarts the gunicorn process, use this
# if you have made changes to templates/start_gunicorn.bash
reload_gunicorn = [

  # Push the gunicorn startup script to server
  {"action":"put_template", "params":{"template":"%(FAB_CONFIG_PATH)s/templates/start_gunicorn.bash",
                                       "destination":"%(PROJECT_PATH)s/start_gunicorn.bash"}},
  {"action":"sudo", "params":"chmod +x %(PROJECT_PATH)s/start_gunicorn.bash"},

  # Restart gunicorn to update the site
  {"action":"sudo", "params": "supervisorctl restart %(PROJECT_NAME)s"}
]

# Pushes the nginx config files to the servers and restarts the nginx, use this if you
# have made changes to templates/nginx-app-proxy or templates/nginx.conf
reload_nginx = [

  # stop old nginx process
  {"action":"sudo", "params":"service nginx stop"},

  # Load the nginx config files
  {"action":"put", "params":{"file":"%(FAB_CONFIG_PATH)s/templates/nginx.conf",
    "destination":"/home/%(SERVER_USERNAME)s/nginx.conf"},
    "message":"Configuring nginx"},
  {"action":"sudo", "params":"mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.old"},
  {"action":"sudo", "params":"mv /home/%(SERVER_USERNAME)s/nginx.conf /etc/nginx/nginx.conf"},
  {"action":"sudo", "params":"chown root:root /etc/nginx/nginx.conf"},
  {"action":"put_template", "params":{"template":"%(FAB_CONFIG_PATH)s/templates/nginx-app-proxy",
                                      "destination":"/home/%(SERVER_USERNAME)s/%(PROJECT_NAME)s"}},
  {"action":"sudo", "params":"rm -rf /etc/nginx/sites-enabled/default"},
  {"action":"sudo", "params":"mv /home/%(SERVER_USERNAME)s/%(PROJECT_NAME)s /etc/nginx/sites-available/%(PROJECT_NAME)s"},
  # {"action":"sudo", "params":"ln -s /etc/nginx/sites-available/%(PROJECT_NAME)s /etc/nginx/sites-enabled/%(PROJECT_NAME)s"},
  {"action":"sudo", "params":"chown root:root /etc/nginx/sites-available/%(PROJECT_NAME)s"},
  {"action":"sudo", "params":"/etc/init.d/nginx restart", "message":"Restarting nginx"},
]

# Pushes the supervisor config files to the servers and restarts the supervisor, use this if you
# have made changes to templates/supervisord-init or templates/supervisord.conf
reload_supervisor = [

  # stop old supervisor process
  {"action":"sudo", "params":"supervisorctl stop all"},
  {"action":"sudo", "params":"killall supervisord"},

  # Setup supervisor
  {"action":"run", "params":"echo_supervisord_conf > /home/%(SERVER_USERNAME)s/supervisord.conf",
    "message":"Configuring supervisor"},
  {"action":"put_template", "params":{"template":"%(FAB_CONFIG_PATH)s/templates/supervisord.conf",
                                      "destination":"/home/%(SERVER_USERNAME)s/my.supervisord.conf"}},
  {"action":"run", "params":"cat /home/%(SERVER_USERNAME)s/my.supervisord.conf >> /home/%(SERVER_USERNAME)s/supervisord.conf"},
  {"action":"run", "params":"rm /home/%(SERVER_USERNAME)s/my.supervisord.conf"},
  {"action":"sudo", "params":"mv /home/%(SERVER_USERNAME)s/supervisord.conf /etc/supervisord.conf"},
  {"action":"sudo", "params":"supervisord"},
  {"action":"put", "params":{"file":"%(FAB_CONFIG_PATH)s/templates/supervisord-init",
                            "destination":"/home/%(SERVER_USERNAME)s/supervisord-init"}},
  {"action":"sudo", "params":"mv /home/%(SERVER_USERNAME)s/supervisord-init /etc/init.d/supervisord"},
  {"action":"sudo", "params":"chmod +x /etc/init.d/supervisord"},
  {"action":"sudo", "params":"update-rc.d supervisord defaults"},

  # Restart supervisor
  {"action":"sudo", "params":"supervisorctl start all"}
]
