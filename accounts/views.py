from __future__ import unicode_literals
from django.core.urlresolvers import reverse_lazy
from django.views import generic
from django.contrib.auth import get_user_model
from django.contrib import auth
from django.contrib import messages
from authtools import views as authviews
from django.contrib.auth import logout
from braces import views as bracesviews
from django.conf import settings
from . import forms
from profiles.models import Profile
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect

from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from utils import send_activatation_email
from utils import send_after_activate_mail
from utils import generate_activation_key

User = get_user_model()


class LoginView(bracesviews.AnonymousRequiredMixin,
                authviews.LoginView):
    form_class = forms.LoginForm
    template_name = 'accounts/login.html'

    def form_valid(self, form):
        redirect = super(LoginView, self).form_valid(form)
        remember_me = form.cleaned_data.get('remember_me')
        if remember_me is True:
            ONE_MONTH = 30 * 24 * 60 * 60
            expiry = getattr(settings, "KEEP_LOGGED_DURATION", ONE_MONTH)
            self.request.session.set_expiry(expiry)
        return redirect


class LogoutView(authviews.LogoutView):
    url = reverse_lazy('accounts:login')



class SignUpView(generic.TemplateView):
    template_name = "accounts/signup.html"
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        kwargs['api_url'] = settings.API_ENDPOINT
        return super(SignUpView, self).get(self, *args, **kwargs)

class ScheduleDemoSuccessView(generic.TemplateView):
    template_name = "accounts/scheduledemo_success.html"

class PasswordChangeView(authviews.PasswordChangeView):
    form_class = forms.PasswordChangeForm
    template_name = 'accounts/password-change.html'
    success_url = reverse_lazy("accounts:login")

    def form_valid(self, form):
        form.save()
        logout(self.request)
        messages.success(self.request,
                         "Your password was changed, "
                         "hence you have been logged out. Please relogin")
        return super(PasswordChangeView, self).form_valid(form)


class PasswordResetView(authviews.PasswordResetView):
    form_class = forms.PasswordResetForm
    template_name = 'accounts/password-reset.html'
    success_url = reverse_lazy('accounts:password-reset-done')
    subject_template_name = 'accounts/emails/password-reset-subject.txt'
    email_template_name = 'accounts/emails/password-reset-email.html'


class PasswordResetDoneView(authviews.PasswordResetDoneView):
    template_name = 'accounts/password-reset-done.html'


class PasswordResetConfirmView(authviews.PasswordResetConfirmAndLoginView):
    template_name = 'accounts/password-reset-confirm.html'
    form_class = forms.SetPasswordForm
