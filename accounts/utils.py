from django.conf import settings
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import hashlib
import random
from django.template import RequestContext,Context
from django.template.loader import get_template



def send_activatation_email(request,activation_key,name,user_email):
    """
    Sending activation code via email
    @input activation_key, user_email
    """
    data = dict()
    protocol ='https://' if request.is_secure() else 'http://'
    host = request.get_host()
    # data['body'] ="Activate your chromo using this link " + protocol+request.get_host() +'/activate/'+activation_key
    # sending params to the template
    params= RequestContext(request,{'protocol':protocol,'host':host,'activation_key':activation_key,'name':name,'email':user_email})
    message = get_template('accounts/activate_email.html').render(params)
    data['html'] = message
    # print (data['html'])
    data['subject']= "Action required: Confirm your Affect Lab account"
    data['to']= user_email
    send_email(data)

def send_after_activate_mail(user_email):
    data = dict()
    data['subject'] = "Thanks for Your Activation"
    data['to'] = user_email
    message = get_template('accounts/signup_new.html').render(Context())
    data['html'] = message
    # print (data['html'])
    # send_email(data)



def send_email(data):
    """
    Generalized Send Email Function which sends email
     @input to_address, subject, body ,html tag
    """
    fromaddr = settings.DEFAULT_FROM_EMAIL
    toaddrs  = data['to']
    message = MIMEMultipart()
    message['from'] = fromaddr
    message['subject'] = data['subject']
    message['to'] = data['to']
    if data.get('html'):
        part = MIMEText(data['html'], 'html')
        message.attach(part)
    else:
        message.attach(MIMEText(data['body'], 'plain'))
    smtp_server = settings.EMAIL_HOST
    smtp_username =settings.EMAIL_HOST_USER
    smtp_password = settings.EMAIL_HOST_PASSWORD
    smtp_port = settings.EMAIL_PORT
    smtp_do_tls = True

    server = smtplib.SMTP(
        host = smtp_server,
        port = smtp_port,
        timeout = 10
    )
    server.starttls()
    server.ehlo()
    server.login(smtp_username, smtp_password)
    server.sendmail(fromaddr, toaddrs, message.as_string())
    server.quit()



def generate_activation_key(username):
    """
    Generation of activation key using random salt based on username
    @input username
    @output activation_key
    """

    salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
    usernamesalt = username
    if isinstance(usernamesalt, unicode):
        usernamesalt = usernamesalt.encode('utf8')
    activation_key = hashlib.sha1(salt+usernamesalt).hexdigest()
    return activation_key