from __future__ import unicode_literals
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions
from authtools import forms as authtoolsforms
from django.contrib.auth import forms as authforms
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.conf import settings


class LoginForm(AuthenticationForm):
    remember_me = forms.BooleanField(label="Remember Me?", required=False, initial=False)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = True
        self.fields["username"].widget.input_type = "text"  # ugly hack
        self.fields['username'].label = ""
        self.fields['password'].label = ""
        self.fields['remember_me'].label = "Remember Me"


        self.helper.layout = Layout(
            Field('username', placeholder="Email", autofocus=""),
            Field('password', placeholder="Password"),
            HTML('<a href="{}">Forgot Password?</a>'.format(
                reverse("accounts:password-reset"))),
            Field('remember_me',css_class="check"),

            Submit('sign_in', 'Log in',
                   css_class="login"),
            )


class SignupForm(authtoolsforms.UserCreationForm):
    phone_number_errors = {
    'required': 'This field is required',
    'invalid': 'Enter valid phone number, min length 5 and max length 15.'
    }
    phone_number = forms.RegexField(regex=r'^\+?\d{5,15}$', error_messages = phone_number_errors)
    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields["email"].widget.input_type = "text"  # ugly hack

        self.helper.layout = Layout(
            Field('username', placeholder="Enter Full Name", autofocus=""),
            Field('email', placeholder="Enter Email"),
            Field('password1', placeholder="Enter Password"),
            Field('password2', placeholder="Re-enter Password"),
            Field('phone_number', placeholder="Enter Phone Number"),
            Submit('sign_up', 'Sign up', css_class="btn btn-lg btn-primary btn-block"),
            )

    def save(self, is_active=False):
        """
        Saving the user instance with status as inactive
        """
        user = super(SignupForm, self).save(commit=False)
        user.is_active=is_active
        user.save()
        return user


class PasswordChangeForm(authforms.PasswordChangeForm):

    def __init__(self, *args, **kwargs):
        super(PasswordChangeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        # This form will be displayed for change password button
        # cancel button is included for going back url.
        self.helper.layout = Layout(
            Field('old_password', placeholder="Enter old password",autofocus=""),
            Field('new_password1', placeholder="Enter new password"),
            Field('new_password2', placeholder="Enter new password (again)"),
            Submit('pass_change', 'Change Password', css_class="btn btn-lg btn-success btn-inline"),
            Button('cancel', 'Cancel',onclick='history.go(-1);',css_class="btn btn-lg btn-danger btn-inline"),
            )


class PasswordResetForm(authtoolsforms.FriendlyPasswordResetForm):

    def __init__(self, *args, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields["email"].widget.input_type = "text"  # ugly hack

        self.helper.layout = Layout(
            Field('email', placeholder="Enter email",
                  autofocus=""),
            Submit('pass_reset', 'Reset Password', css_class="login"),
            )


class SetPasswordForm(authforms.SetPasswordForm):
    def __init__(self, *args, **kwargs):
        super(SetPasswordForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.layout = Layout(
            Field('new_password1', placeholder="Enter new password",
                  autofocus=""),
            Field('new_password2', placeholder="Enter new password (again)"),
            Submit('pass_change', 'Change Password', css_class="btn btn-lg btn-primary btn-block"),
            )



