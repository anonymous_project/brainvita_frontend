//
//
//
// // Graph line code
//
// var margin = {top: 0, right: 0, bottom: 0, left: 0},
//     width =(window.innerWidth/100)*35.35,
//     height = (window.innerHeight/100)*51.6,
//     padding = 100;
//
// var dataset = [
//     {x: 5, y: 21},
//     {x: 6, y: 18},
//     {x: 7, y: 23},
//     {x: 8, y: 24},
//     {x: 9, y: 28},
//     {x: 10, y: 35},
//     {x: 11, y: 30},
//     {x: 12, y: 32},
//     {x: 13, y: 36},
//     {x: 14, y: 40},
//     {x: 15, y: 10},
//     {x: 16, y: 11},
//     {x: 17, y: 20},
//     {x: 18, y: 27},
//     {x: 19, y: 10},
//     {x: 20, y: 10},
//     {x: 21, y: 13},
//     {x: 22, y: 12},
//     {x: 23, y: 11},
//     {x: 24, y: 10},
//     {x: 25, y: 34},
//     {x: 26, y: 27},
//     {x: 27, y: 19},
//     {x: 28, y: 20},
//     {x: 29, y: 39},
//     {x: 30, y: 44},
//     {x: 31, y: 35},
//     {x: 32, y: 22},
//     {x: 33, y: 20},
//     {x: 34, y: 10},
//     {x: 35, y: 39},
//     {x: 36, y: 49},
//     {x: 37, y: 43},
//     {x: 38, y: 45},
//     {x: 39, y: 32},
//     {x: 40, y: 30},
//     {x: 41, y: 20},
//     {x: 42, y: 44},
//     {x: 43, y: 51},
//     {x: 44, y: 43},
//     {x: 45, y: 60},
//     {x: 46, y: 53},
//     {x: 47, y: 55},
//     {x: 48, y:36},
//     {x: 49, y: 17},
//     {x: 50, y: 30},
//     {x: 52, y: 58},
//     {x: 53, y: 16},
//     {x: 54, y: 25},
//     {x: 55, y: 24},
//     {x: 56, y: 27},
//     {x: 57, y: 34},
//     {x: 58, y: 42},
//     {x: 59, y: 47},
//     {x: 60, y: 33}
// ];
// // var axis = d3.axisLeft(scale);
// var xScale = d3.scale.linear()
//     .domain([0, d3.max(dataset, function(d){ return d.x; })])
//     .range([0, width]);
//
// var yScale = d3.scale.linear()
//     .domain([0, d3.max(dataset, function(d){ return d.y; })])
//     .range([height, 0]);
//
// var xAxis = d3.svg.axis()
//     .scale(xScale)
//     .orient("bottom")
//     .innerTickSize(-height)
//     .outerTickSize(0)
//     .tickPadding(5);
//
// var yAxis = d3.svg.axis()
//     .scale(yScale)
//     .orient("left")
//     .innerTickSize(-width)
//     .outerTickSize(0)
//     .tickPadding(5);
//
// var line = d3.svg.line()
//     .x(function(d) { return xScale(d.x); })
//     .y(function(d) { return yScale(d.y); });
//
// var svg = d3.select("#line")
//     .append("svg:svg")
//     .attr("width", width)
//     .attr("height", height)
//     .attr("id", "visualization")
//     .attr("xmlns", "http://www.w3.org/2000/svg");
//
//
// svg.append("svg:g")
//     .attr("class", "grid")
//     .attr("class", "x axis")
//     .attr("transform", "translate("+30+",355)")
//     .call(xAxis);
// svg.append("svg:g")
//     .attr("class", "grid")
//     .attr("class", "y axis")
//     .attr("transform", "translate("+30+",6)")
//     .call(yAxis);
// svg.append("text")
//     .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
//     .attr("transform", "translate("+ (10) +","+(height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
//     .text("Score");
//
// svg.append("text")
//     .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
//     .attr("transform", "translate("+ (width/2) +","+(height-(-50))+")")  // centre below axis
//     .text("Time");
//
// // var path = svg.append("path")
// //     .attr("d", line(dataset))
// //     .attr("stroke", "steelblue")
// //     .attr("stroke-width", "3")
// //     .attr("fill", "none");
// //
// // var totalLength = path.node().getTotalLength();
// //
// // path
// //     .attr("stroke-dasharray", totalLength + " " + totalLength)
// //     .attr("stroke-dashoffset", totalLength)
// //     .transition()
// //     .duration(82000)
// //     .ease("linear")
// //     .attr("stroke-dashoffset", 0);
// //
// // svg.on("click", function(){
// //     path
// //         .transition()
// //         .duration(82000)
// //         .ease("linear")
// //         .attr("stroke-dashoffset", totalLength);
// // });
// // //
// // }
//
//
//
// var vid = document.getElementById("myVideo");
// var selectedEmotion = {};
// selectedEmotion.Happy  = {};
// selectedEmotion.Happy.isSelected  = "True";
// selectedEmotion.Happy.dataSet  = [
//     {x:4, y:3},
//     {x: 4, y: 10},
//     {x: 6, y: 13},
//     {x: 7, y: 12},
//     {x: 4, y: 16},
//     {x: 5, y: 21},
//     {x: 6, y: 18},
//     {x: 7, y: 23},
//     {x: 8, y: 24},
//     {x: 9, y: 28},
//     {x: 10, y: 35},
//     {x: 11, y: 30},
//     {x: 12, y: 32},
//     {x: 13, y: 36},
//     {x: 14, y: 40},
//     {x: 15, y: 10},
//     {x: 16, y: 11},
//     {x: 17, y: 20},
//     {x: 18, y: 27},
//     {x: 19, y: 40},
//     {x: 20, y: 10},
//     {x: 21, y: 13},
//     {x: 22, y: 12},
//     {x: 23, y: 11},
//     {x: 24, y: 10},
//     {x: 25, y: 34},
//     {x: 26, y: 27},
//     {x: 27, y: 19},
//     {x: 28, y: 20},
//     {x: 29, y: 39},
//     {x: 30, y: 44},
//     {x: 31, y: 35},
//     {x: 32, y: 22},
//     {x: 33, y: 20},
//     {x: 34, y: 10},
//     {x: 35, y: 59},
//     {x: 36, y: 49},
//     {x: 37, y: 43},
//     {x: 38, y: 45},
//     {x: 39, y: 32},
//     {x: 40, y: 30},
//     {x: 41, y: 30},
//     {x: 42, y: 44},
//     {x: 43, y: 51},
//     {x: 44, y: 43},
//     {x: 45, y: 50},
//     {x: 46, y: 33},
//     {x: 47, y: 35},
//     {x: 48, y: 26},
//     {x: 49, y: 17},
//     {x: 50, y: 20},
//     {x: 52, y: 58},
//     {x: 53, y: 16},
//     {x: 54, y: 25},
//     {x: 55, y: 24},
//     {x: 56, y: 27},
//     {x: 57, y: 34},
//     {x: 58, y: 42},
//     {x: 59, y: 47},
//     {x: 60, y: 33}
// ];
// selectedEmotion.Sad  = {};
// selectedEmotion.Sad.isSelected  = "True";
// selectedEmotion.Sad.dataSet  = [
//     {x: 4, y: 6},
//     {x: 6, y: 7},
//     {x: 9, y: 40},
//     {x: 10, y: 12},
//     {x: 4, y: 2},
//     {x: 5, y: 21},
//     {x: 6, y: 22},
//     {x: 7, y: 23},
//     {x: 8, y: 35},
//     {x: 9, y: 37},
//     {x: 10, y: 2},
//     {x: 11, y: 22},
//     {x: 12, y: 32},
//     {x: 13, y: 36},
//     {x: 14, y: 40},
//     {x: 15, y: 10},
//     {x: 16, y: 11},
//     {x: 17, y: 20},
//     {x: 18, y: 27},
//     {x: 19, y: 60},
//     {x: 20, y: 10},
//     {x: 21, y: 13},
//     {x: 22, y: 12},
//     {x: 23, y: 11},
//     {x: 24, y: 10},
//     {x: 25, y: 34},
//     {x: 26, y: 27},
//     {x: 27, y: 19},
//     {x: 28, y: 20},
//     {x: 29, y: 39},
//     {x: 30, y: 44},
//     {x: 31, y: 35},
//     {x: 32, y: 22},
//     {x: 33, y: 20},
//     {x: 34, y: 10},
//     {x: 35, y: 59},
//     {x: 36, y: 49},
//     {x: 37, y: 43},
//     {x: 38, y: 45},
//     {x: 39, y: 32},
//     {x: 40, y: 30},
//     {x: 41, y: 60},
//     {x: 42, y: 44},
//     {x: 43, y: 51},
//     {x: 44, y: 43},
//     {x: 45, y: 50},
//     {x: 46, y: 53},
//     {x: 47, y: 55},
//     {x: 48, y: 56},
//     {x: 49, y: 57},
//     {x: 50, y: 60},
//     {x: 52, y: 58},
//     {x: 53, y: 16},
//     {x: 54, y: 25},
//     {x: 55, y: 24},
//     {x: 56, y: 27},
//     {x: 57, y: 34},
//     {x: 58, y: 42},
//     {x: 59, y: 47},
//     {x: 60, y: 33}
// ];
// selectedEmotion.Angry={};
// selectedEmotion.Angry.isSelected  = "True";
// selectedEmotion.Angry.dataSet  = [
//     {x: 4, y: 10},
//     {x: 1, y: 52},
//     {x: 2, y: 3},
//     {x: 3, y: 4},
//     {x: 4, y: 5},
//     {x: 5, y: 67},
//     {x: 6, y: 7},
//     {x: 7, y: 23},
//     {x: 8, y: 8},
//     {x: 9, y: 9},
//     {x: 10, y: 0},
//     {x: 11, y: 8},
//     {x: 12, y: 6},
//     {x: 13, y: 36},
//     {x: 14, y: 40},
//     {x: 15, y: 10},
//     {x: 16, y: 11},
//     {x: 17, y: 20},
//     {x: 18, y: 27},
//     {x: 19, y: 60},
//     {x: 20, y: 10},
//     {x: 21, y: 13},
//     {x: 22, y: 12},
//     {x: 23, y: 11},
//     {x: 24, y: 10},
//     {x: 25, y: 34},
//     {x: 26, y: 27},
//     {x: 27, y: 19},
//     {x: 28, y: 20},
//     {x: 29, y: 39},
//     {x: 30, y: 44},
//     {x: 31, y: 35},
//     {x: 32, y: 22},
//     {x: 33, y: 20},
//     {x: 34, y: 10},
//     {x: 35, y: 59},
//     {x: 36, y: 49},
//     {x: 37, y: 43},
//     {x: 38, y: 45},
//     {x: 39, y: 32},
//     {x: 40, y: 30},
//     {x: 41, y: 60},
//     {x: 42, y: 44},
//     {x: 43, y: 51},
//     {x: 44, y: 43},
//     {x: 45, y: 50},
//     {x: 46, y: 53},
//     {x: 47, y: 55},
//     {x: 48, y: 56},
//     {x: 49, y: 57},
//     {x: 50, y: 60},
//     {x: 52, y: 58},
//     {x: 53, y: 16},
//     {x: 54, y: 25},
//     {x: 55, y: 24},
//     {x: 56, y: 27},
//     {x: 57, y: 34},
//     {x: 58, y: 42},
//     {x: 59, y: 47},
//     {x: 60, y: 33}
// ];
// function playVid() {
//     vid.play();
//     dataPlot();
// }
//
// function attention(){
//     attentiondata();
// }
// function meditation(){
//     attentiondata();
// }
//
// function attentiondata() {
//
//     d3.select("svg").remove();
//
//     // d3.select("svg").append("g");
//
//     // d3.select("svg").append();
//     var margin = {top: 0, right: 0, bottom: 0, left: 0},
//         width = 460 - margin.left - margin.right,
//         height = 350 - margin.top - margin.bottom,
//         padding = 100;
//
//     // var axis = d3.axisLeft(scale);
//     var xScale = d3.scale.linear()
//         .domain([0, d3.max(dataset, function(d){ return d.x; })])
//         .range([0, width]);
//
//     var yScale = d3.scale.linear()
//         .domain([0, d3.max(dataset, function(d){ return d.y; })])
//         .range([height, 0]);
//
//     var xAxis = d3.svg.axis()
//         .scale(xScale)
//         .orient("bottom")
//         .innerTickSize(-height)
//         .outerTickSize(0)
//         .tickPadding(10);
//
//     var yAxis = d3.svg.axis()
//         .scale(yScale)
//         .orient("left")
//         .innerTickSize(-width)
//         .outerTickSize(0)
//         .tickPadding(5);
//
//
//     var line = d3.svg.line()
//         .x(function(d) { return xScale(d.x); })
//         .y(function(d) { return yScale(d.y); });
//
//
//
//     var svg = d3.select("#line")
//         .append("svg:svg")
//         .attr("width", width)
//         .attr("height", height)
//         .attr("id", "visualization")
//         .attr("xmlns", "http://www.w3.org/2000/svg");
//
//
//     svg.append("svg:g")
//         .attr("class", "x axis")
//         .attr("transform", "translate("+30+",355)")
//         .call(xAxis);
//     svg.append("svg:g")
//         .attr("class", "y axis")
//         .attr("transform", "translate("+30+",6)")
//         .call(yAxis);
//     svg.append("text")
//         .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
//         .attr("transform", "translate("+ (10) +","+(height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
//         .text("Score");
//
//     svg.append("text")
//         .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
//         .attr("transform", "translate("+ (width/2) +","+(height-(-50))+")")  // centre below axis
//         .text("Time");
//
//     // Define the div for the tooltip
//     // Add the scatterplot
//     // Define the div for the tooltip
//     var div = d3.select("body").append("div")
//         .attr("class", "tooltip")
//         .style("opacity", 0);
//     svg.selectAll("dot")
//         .data(dataset)
//         .enter().append("circle")
//         .style("stroke", "white")
//         .style("fill", "white")
//         .attr("r",3)
//         .attr("cx", function(d) { return xScale(d.x); })
//         .attr("cy", function(d) { return yScale(d.y); })
//         .on("mouseover", function(d) {
//             div.transition()
//                 .duration(200)
//                 .style("opacity", .9);
//             div	.html(xScale(d.x) + "<br/>"  + d.y,yScale(d.y) + "<br/>"  + d.x)
//                 .style("left", (d3.event.pageX) + "px")
//                 .style("top", (d3.event.pageY - 28) + "px");
//         })
//         .on("mouseout", function(d) {
//             div.transition()
//                 .duration(500)
//                 .style("opacity", 0);
//         });
//     if(selectedEmotion.Happy.isSelected == "False"){
//         addLine(svg,selectedEmotion.Happy.dataSet,"steelblue")
//     }
//     if(selectedEmotion.Sad.isSelected == "False"){
//         addLine(svg,selectedEmotion.Sad.dataSet,"red")
//     }
//     if(selectedEmotion.Sad.isSelected  == "True"){
//         addLine(svg,selectedEmotion.Sad.dataSet,"orange")
//     }
// }
//
// function dataPlot() {
//     d3.select("svg").remove();
//
//     // d3.select("svg").append("g");
//
//     // d3.select("svg").append();
//     var margin = {top: 0, right: 0, bottom: 0, left: 0},
//         width = 460 - margin.left - margin.right,
//         height = 350 - margin.top - margin.bottom,
//         padding = 100;
//
//     // var axis = d3.axisLeft(scale);
//     var xScale = d3.scale.linear()
//         .domain([0, d3.max(dataset, function(d){ return d.x; })])
//         .range([0, width]);
//
//     var yScale = d3.scale.linear()
//         .domain([0, d3.max(dataset, function(d){ return d.y; })])
//         .range([height, 0]);
//
//     var xAxis = d3.svg.axis()
//         .scale(xScale)
//         .orient("bottom")
//         .innerTickSize(-height)
//         .outerTickSize(0)
//         .tickPadding(10);
//
//     var yAxis = d3.svg.axis()
//         .scale(yScale)
//         .orient("left")
//         .innerTickSize(-width)
//         .outerTickSize(0)
//         .tickPadding(5);
//
//
//     var line = d3.svg.line()
//         .x(function(d) { return xScale(d.x); })
//         .y(function(d) { return yScale(d.y); });
//
//
//
//     var svg = d3.select("#line")
//         .append("svg:svg")
//         .attr("width", width)
//         .attr("height", height)
//         .attr("id", "visualization")
//         .attr("xmlns", "http://www.w3.org/2000/svg");
//
//
//     svg.append("svg:g")
//         .attr("class", "x axis")
//         .attr("transform", "translate("+30+",355)")
//         .call(xAxis);
//     svg.append("svg:g")
//         .attr("class", "y axis")
//         .attr("transform", "translate("+30+",6)")
//         .call(yAxis);
//     svg.append("text")
//         .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
//         .attr("transform", "translate("+ (10) +","+(height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
//         .text("Score");
//
//     svg.append("text")
//         .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
//         .attr("transform", "translate("+ (width/2) +","+(height-(-50))+")")  // centre below axis
//         .text("Time");
//
//     // Define the div for the tooltip
//     // Add the scatterplot
//     // Define the div for the tooltip
//     var div = d3.select("body").append("div")
//         .attr("class", "tooltip")
//         .style("opacity", 0);
//     svg.selectAll("dot")
//         .data(dataset)
//         .enter().append("circle")
//         .style("stroke", "white")
//         .style("fill", "white")
//         .attr("r",3)
//         .attr("cx", function(d) { return xScale(d.x); })
//         .attr("cy", function(d) { return yScale(d.y); })
//         .on("mouseover", function(d) {
//             div.transition()
//                 .duration(200)
//                 .style("opacity", .9);
//             div	.html(xScale(d.x) + "<br/>"  + d.y,yScale(d.y) + "<br/>"  + d.x)
//                 .style("left", (d3.event.pageX) + "px")
//                 .style("top", (d3.event.pageY - 28) + "px");
//         })
//         .on("mouseout", function(d) {
//             div.transition()
//                 .duration(500)
//                 .style("opacity", 0);
//         });
//     if(selectedEmotion.Happy.isSelected == "True"){
//         addLine(svg,selectedEmotion.Happy.dataSet,"steelblue")
//     }
//     if(selectedEmotion.Sad.isSelected == "True"){
//         addLine(svg,selectedEmotion.Sad.dataSet,"red")
//     }
//     if(selectedEmotion.Angry.isSelected  == "False"){
//         addLine(svg,selectedEmotion.Angry.dataSet,"orange")
//     }
// }
// function addLine(svg,dataset,color) {
//     var path = svg.append("path")
//         .attr("d", line(dataset))
//         .attr("stroke", color)
//         .attr("stroke-width", "1")
//         .attr("fill", "none");
//     var totalLength = path.node().getTotalLength();
//
//     path
//         .attr("stroke-dasharray", totalLength + " " + totalLength)
//         .attr("stroke-dashoffset", totalLength)
//         .transition()
//         .duration(60000)
//         .ease("linear")
//         .attr("stroke-dashoffset", 0);
// }
//
// function pauseVid() {
//     vid.pause();
//
// }
// $( "#stop" ).click(function() {
//     $( "path" ).remove();
//     var path = svg.append("path")
//         .attr("d", line(dataset))
//         .attr("stroke", "black")
//         .attr("stroke-width", "1")
//         .attr("fill", "none");
//     var totalLength = path.node().getTotalLength();
//
//     path
//         .attr("stroke-dasharray", totalLength + " " + totalLength)
//         .attr("stroke-dashoffset", totalLength)
//         .transition()
//         .duration(0)
//         .ease("linear")
//         .attr("stroke-dashoffset", 0);
//
//
//     // d3.selectAll("dot").clear();
// });
//
// $("button").on("click",function (){
//     $(this).toggleClass("selected_param");
// });
// //
// // $(document).ready(function(){
// //     $('.nav li a').click(function(){
// //         $('.nav li a').removeClass("active");
// //         $(this).addClass("active");
// //     });
// //
// // });
//
// $(function() {
//     var e = window.location.pathname,
//     a = e.substring(e.lastIndexOf("/"));
//     $(".nav li a").each(function() {
//         var e = this.href.substring(this.href.lastIndexOf("/"));
//         a == e && ($(".nav li a").removeClass("active"), $(this).parent().addClass("active"))
//     })
// });
// // jQuery(document).ready(function($){
// //     // Get current url
// //     // Select an a element that has the matching href and apply a class of 'active'. Also prepend a - to the content of the link
// //     var url = window.location.href;
// //     $('.nav li a[href="'+url+'"]').addClass('.navbar-nav>li>a.active');
// // });
//
//
//
//
//
//


var player,
    time_update_interval = 0;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('video-placeholder', {
        width: 600,
        height: 400,
        videoId: 'Xa0Q0J5tOP0',
        playerVars: {
            color: 'white',
            playlist: 'taJ60kskkns,FG0fTKAqZ5g'
        },
        events: {
            onReady: initialize
        }
    });
    // player.onseeking = function(){
    //     alert("test")
    // }
}

// $(document).ready(function(){
//     $("#homeScreenVideo").on(
// 	"timeupdate",
// 	function(event){
// 	    onTrackedVideoFrame(this.currentTime, this.duration);
// 	});
// }

// function onTrackedVideoFrame(currentTime, duration){
//     $("#detail").textcss("left",currentTime);
//     $("#duration").text(duration);
//    }

function onSeekingHomeScreenVideo(){
    $(".detail").addClass("active");
    $(".detail").append('<div class="x_label" ><label id="minutes">00</label>:<label id="seconds">00</label></div>');
    $(".detail").append('<div class="item active left" id="x_label" style="top: 0.0px;">Time:</div>');
    $(".item").append('<div class="dot active" style="top: 242.382px; border-color: rgb(47, 37, 74);top: 10px; left:-8px;"></div>');

    // $(".detail").children().find(".dot").addClass("active");
    var videoLength = document.getElementById("homeScreenVideo").duration;
    var counter = 1;
    var currentTime = 0;

    width = $("#chart").width();
    var timer = setInterval(function () {
        $(".detail").css("left",counter);

	$("#homeScreenVideo").on(
	"timeupdate",
	function(event){
	    counter =((this.currentTime/parseFloat(this.duration))*width) ;
         $(".detail").append('<div class="item active left" id="x_label" style="top: 0.0px;">Time:</div>');
         document.getElementById("x_label").textContent = "Time : " + Math.round(this.currentTime);
        //document.querySelector("item active left").textContent = "Attention : HUh";
        //document.getElementsByClassName("item active left").
        //console.log(document.getElementsByClassName("item active left"));
	    currentTime ++;
	    if(this.currentTime == this.duration){
		clearInterval(timer);
        }

	});
	
        //counter =  counter + 14;

    },1000);
    var minutesLabel = document.getElementById("minutes");
    var secondsLabel = document.getElementById("seconds");
    var totalSeconds = 0;
    setInterval(setTime, 1000);

    function setTime()
    {
        ++totalSeconds;
        secondsLabel.innerHTML = pad(totalSeconds%60);
        minutesLabel.innerHTML = pad(parseInt(totalSeconds/60));
    }

    function pad(val)
    {
        var valString = val + "";
        if(valString.length < 2)
        {
            return valString;
        }
        else
        {
            return valString;
        }
    }
}
var timer = setInterval(function () {
    var counter = 5;
    var currentTime = 0;
    if(document.getElementById("homeScreenVideo")!= null){
        var videoLength = document.getElementById("homeScreenVideo").duration;
        // $(".detail").css("left",counter);
        counter =  counter + 14;
        currentTime ++;
        if(currentTime == videoLength.toFixed(0)){
            clearInterval(timer);
        }
    }
},1000);


function stopCursor() {
    //$(".detail").addClass("inactive")
    clearInterval(timer);
    // onSeekingHomeScreenVideo().stop();
}

function initialize(){

    // Update the controls on load
    updateTimerDisplay();
    updateProgressBar();

    // Clear any old interval.
    clearInterval(time_update_interval);

    // Start interval to update elapsed time display and
    // the elapsed part of the progress bar every second.
    time_update_interval = setInterval(function () {
        updateTimerDisplay();
        updateProgressBar();
    }, 1000);


    $('#volume-input').val(Math.round(player.getVolume()));
}


// This function is called by initialize()
function updateTimerDisplay(){
    // Update current time text display.
    $('#current-time').text(formatTime( player.getCurrentTime() ));
    $('#duration').text(formatTime( player.getDuration() ));
}


// This function is called by initialize()
function updateProgressBar(){
    // Update the value of our progress bar accordingly.
    $('#progress-bar').val((player.getCurrentTime() / player.getDuration()) * 100);
}


// Progress bar

$('#progress-bar').on('mouseup touchend', function (e) {

    // Calculate the new time for the video.
    // new time in seconds = total duration in seconds * ( value of range input / 100 )
    var newTime = player.getDuration() * (e.target.value / 100);

    // Skip video to new time.
    player.seekTo(newTime);

});


// Playback

$('#play').on('click', function () {
    player.playVideo();
    // ("#chart").addClass('.rickshaw_graph .detail');
});


$('#pause').on('click', function () {
    player.pauseVideo();
});


// Sound volume


$('#mute-toggle').on('click', function() {
    var mute_toggle = $(this);

    if(player.isMuted()){
        player.unMute();
        mute_toggle.text('volume_up');
    }
    else{
        player.mute();
        mute_toggle.text('volume_off');
    }
});

$('#volume-input').on('change', function () {
    player.setVolume($(this).val());
});


// Other options


$('#speed').on('change', function () {
    player.setPlaybackRate($(this).val());
});

$('#quality').on('change', function () {
    player.setPlaybackQuality($(this).val());
});


// Playlist

$('#next').on('click', function () {
    player.nextVideo()
});

$('#prev').on('click', function () {
    player.previousVideo()
});


// Load video

$('.thumbnail').on('click', function () {

    var url = $(this).attr('data-video-id');

    player.cueVideoById(url);

});


// Helper Functions

function formatTime(time){
    time = Math.round(time);

    var minutes = Math.floor(time / 60),
        seconds = time - minutes * 60;

    seconds = seconds < 10 ? '0' + seconds : seconds;

    return minutes + ":" + seconds;
}


$('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
});



//
//  $(function() {
//     var e = window.location.pathname,
//     a = e.substring(e.lastIndexOf("/"));
//     $(".nav li a").each(function() {
//         var e = this.href.substring(this.href.lastIndexOf("/"));
//         a == e && ($(".nav li a .active").removeClass("active"), $(this).parent().addClass("active"))
//     })
// });
//
$(document).ready(function() {
  $('.nav li a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
  $(this).removeClass('active');
});

