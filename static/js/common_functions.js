function call_ajax(request_type,request_url,request_data) {
    var data= [];
    try{
        $.ajax({
            type: request_type,
            url: request_url,
            dataType: 'json',
            data:request_data,
            cors:true,
            async:false,
            success: function(response){
                data =response;
                $("#loader").hide();
            }
        });
    }catch(error){
        console.log(error);
    }
    return data;
}
function call_ajax_json(request_type,request_url,request_data) {
    var data= [];
    try{
        $.ajax({
            type: request_type,
            url: request_url,
            dataType: 'json',
            data:request_data,
            cors:true,
            contentType: "application/json",
            async:false,
            success: function(response){
                data =response;
                $("#loader").hide();
            }
        });
    }catch(error){
        console.log(error);
    }
    return data;
}

function ajax_call_with_login(request_type,request_url,request_data) {
    var data= [];
    try{
        $.ajax({
            type: request_type,
            traditional: true,
            url: request_url,
            dataType: 'json',
            data: JSON.stringify(request_data),
            contentType: "application/json",
            cors:true,
            async:false,
            beforeSend: function(xhr) {
                var user = "rajiv";// your actual username
                var pass = "arpit";// your actual password
                var token = user.concat(":", pass);
                //xhr.setRequestHeader('X-CSRF-Token',request_data.csrf_token);
                xhr.setRequestHeader('Authorization', ("Basic ".concat(btoa(token))));
            },
            success: function(response){
                data =response;
                $("#loader").hide();
            }
        });
    }catch(error){
        console.log(error);
    }
    return data;
}
function call_ajax_text(request_type,request_url,request_data) {
    var data= [];
    try{
        $.ajax({
            type: request_type,
            url: request_url,
            dataType: 'text',
            data:request_data,
            cors:true,
            async:false,
            success: function(response){
                data =response;
            }
        });
    }catch(error){
        console.log(error);
    }
    return data;
}