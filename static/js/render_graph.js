function modify_graph(graph,elementlist) {
    var preview = new Rickshaw.Graph.RangeSlider({
        graph: graph,
        element: document.getElementById(elementlist.preview)
    });

    var y_axis = new Rickshaw.Graph.Axis.Y({
        graph: graph,
        tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
        //element: document.getElementById(elementlist.yaxis),
        min:0,
        max:100,
    });
    y_axis.render();

    var hoverDetail = new Rickshaw.Graph.HoverDetail({
        graph: graph,
        xFormatter: function (x) {
            // return new Date(x * 1000).toLocaleString();
            return new Date(x * 1000).toString();
        }
    });

    var smoother = new Rickshaw.Graph.Smoother({
        graph: graph,
        element: document.getElementById(elementlist.smoother)
    });
var formatX = function(x){

        mins = Math.floor(x/60);
        return mins.toString()+":"+(x%60).toString();
   
};

   var xAxis = new Rickshaw.Graph.Axis.X({
        graph: graph,
         ticks:20 ,
        //tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
        tickFormat: function(x){return formatX(x)}
       // element: document.getElementById(elementlist.xaxis)
    });
    xAxis.render();
}
