
var player,
    time_update_interval = 0;
var olderMediaList;
var test_summary_data;

// function onYouTubeIframeAPIReady() {
//     player = new YT.Player('video-placeholder', {
//         width: 600,
//         height: 400,
//         videoId: 'Xa0Q0J5tOP0',
//         playerVars: {
//             color: 'white',
//             playlist: 'taJ60kskkns,FG0fTKAqZ5g'
//         },
//         events: {
//             onReady: initialize
//         }
//     });
// }


function onSeekingHomeScreenVideo(){
    $(".detail").addClass("active");
    $(".detail").append('<div class="x_label" ><label id="minutes">00</label>:<label id="seconds">00</label></div>');
    $(".detail").append('<div class="item active left" id="x_label" style="top: 0.0px;">Time:</div>');
    $(".item").append('<div class="dot active" style="top: 242.382px; border-color: rgb(47, 37, 74);top: 10px; left:-8px;"></div>');

    // $(".detail").children().find(".dot").addClass("active");
    try {
        var videoLength = document.getElementById("homeScreenVideo").duration;
        var counter = 1;
        var currentTime = 0;

        width = $("#chart").width();
        var timer = setInterval(function () {
            $(".detail").css("left", counter);


            $("#homeScreenVideo").on(
                "timeupdate",
                function (event) {
                    counter = Math.round((Math.round(this.currentTime) / Math.round(parseFloat(this.duration))) * width) - 1;
                    $(".detail").append('<div class="item active left" id="x_label" style="top: 0.0px;">Time:</div>');
                    document.getElementById("x_label").textContent = "Time : " + Math.round(this.currentTime);
                    //document.querySelector("item active left").textContent = "Attention : HUh";
                    //document.getElementsByClassName("item active left").
                    //console.log(document.getElementsByClassName("item active left"));
                    currentTime++;
                    if (this.currentTime == this.duration) {
                        clearInterval(timer);
                    }

                });
            //counter =  counter + 14;
        }, Infinity);
        var minutesLabel = document.getElementById("minutes");
        var secondsLabel = document.getElementById("seconds");
        var totalSeconds = 0;
        setInterval(setTime, 1000);
    }
    catch(err)
        {clearInterval(timer);}
    function setTime()
    {
        ++totalSeconds;
        secondsLabel.innerHTML = pad(totalSeconds%60);
        minutesLabel.innerHTML = pad(parseInt(totalSeconds/60));
    }

    function pad(val)
    {
        var valString = val + "";
        if(valString.length < 2)
        {
            return valString;
        }
        else
        {
            return valString;
        }
    }
}

// var timer = setInterval(function () {
//     var counter = 5;
//     var currentTime = 0;
//     try {
//         var videoLength = document.getElementById("homeScreenVideo").duration;
//  // $(".detail").css("left",counter);
//     counter =  counter + 14;
//     currentTime ++;
//     if(currentTime == videoLength.toFixed(0)){
//         clearInterval(timer);
//     }
//     }
//     catch(err)
//     {clearInterval(timer);}
//
// },Infinity);
var chart_object_ref =  {
    emotion:[],
    radar:[],
    distribution:[],
}
function stopCursor() {
    //$(".detail").addClass("inactive")
    clearInterval(timer);
    // onSeekingHomeScreenVideo().stop();
}

function initialize(){
    // disable current this
    return false;
    // Update the controls on load
    updateTimerDisplay();
    updateProgressBar();

    // Clear any old interval.
    clearInterval(time_update_interval);

    // Start interval to update elapsed time display and
    // the elapsed part of the progress bar every second.
    time_update_interval = setInterval(function () {
        updateTimerDisplay();
        updateProgressBar();
    }, 1000);
    $('#volume-input').val(Math.round(player.getVolume()));
}


// This function is called by initialize()
function updateTimerDisplay(){
    // Update current time text display.
    $('#current-time').text(formatTime( player.getCurrentTime() ));
    $('#duration').text(formatTime( player.getDuration() ));
}


// This function is called by initialize()
function updateProgressBar(){
    // Update the value of our progress bar accordingly.
    $('#progress-bar').val((player.getCurrentTime() / player.getDuration()) * 100);
}


// Progress bar

$('#progress-bar').on('mouseup touchend', function (e) {

    // Calculate the new time for the video.
    // new time in seconds = total duration in seconds * ( value of range input / 100 )
    var newTime = player.getDuration() * (e.target.value / 100);

    // Skip video to new time.
    player.seekTo(newTime);

});


// Playback

$('#play').on('click', function () {
    player.playVideo();
    // ("#chart").addClass('.rickshaw_graph .detail');
});


$('#pause').on('click', function () {
    player.pauseVideo();
});


// Sound volume


$('#mute-toggle').on('click', function() {
    var mute_toggle = $(this);

    if(player.isMuted()){
        player.unMute();
        mute_toggle.text('volume_up');
    }
    else{
        player.mute();
        mute_toggle.text('volume_off');
    }
});

$('#volume-input').on('change', function () {
    player.setVolume($(this).val());
});


// Other options

$('#speed').on('change', function () {
    player.setPlaybackRate($(this).val());
});

$('#quality').on('change', function () {
    player.setPlaybackQuality($(this).val());
});


// Playlist

$('#next').on('click', function () {
    player.nextVideo()
});

$('#prev').on('click', function () {
    player.previousVideo()
});


// Load video

$('.thumbnail').on('click', function () {

    var url = $(this).attr('data-video-id');
    player.cueVideoById(url);

});


// Helper Functions

function formatTime(time){
    time = Math.round(time);

    var minutes = Math.floor(time / 60),
        seconds = time - minutes * 60;

    seconds = seconds < 10 ? '0' + seconds : seconds;

    return minutes + ":" + seconds;
}


$('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
});



$(document).ready(function() {
  $('.nav li a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
  $(this).removeClass('active');
});


$(function () {
     if($(location).attr('pathname')== "/users/dashboard/"){
                var selected =$("#dashboard");
                selected.addClass("selected_url");
     }



    function replaceAll(str, find, replace) {
     return str.replace(new RegExp(find, 'g'), replace);
    }

    function populate_graph() {
        $("#chart_wrapper").show();

        //get selected params, metrics, age, gender filters
        var params_data = [];

        var params_mappers = [];
        var gender_filter = "";
        $(".research_parameters .selected_param").each(function(i, v){
            params_data[i] = $(this).attr("val");
            params_mappers[i] = $(this).text();
        });
        var metrics = $(".metrics-wrapper").find(".selected_param").attr("val");
        var age_val = $("#age_dd").children(":selected").attr("val");
        var gender_val =  $("#gender_dd").children(":selected").attr("val");

        var age_filter = "";
        var json_data = {};
        switch(gender_val){
            case "all": gender_filter = "all";break;
            case "versus": gender_filter = "versus";break;
            case "male":gender_filter = ["male"];break;
            case "female":gender_filter = ["female"];break;
            default:gender_filter = "all";break;
        }
        switch(age_val){
            case "below": age_filter = {"op":"lte","params":29};json_data['age']= [age_filter];break;
            case "average": age_filter = {"op":"range","params":[30,49]};json_data['age']= [age_filter];break;
            case "above":age_filter = {"op":"gte","params":50};json_data['age']= [age_filter];break;
            case "versus":break;
            default:age_filter = {"op":"all"};json_data['age']= [age_filter];break;
        }
        json_data['gender']= gender_filter;
        if(age_val !="all" && age_val != "versus"){
            age_val = [parseInt(age_val)];
        }
        var ajax_data = {
             "age":age_val,
             "gender":gender_filter,
             "media_id":media_id

        };
        //end of setting the json data
        if(media_dd.find("option").length > 0)
        {
            // if the media dropdown have any media, it will come here.
            medias = Array.from($('#media_dd').find("option:selected"));
            users =  Array.from($('#user_dd').find("option:selected"));
            var media_fn = medias => medias.map(rec => parseInt(rec['id']));
            var user_fn = users => users.map(rec => rec['id'].split("_")[1]);
            medias=media_fn(medias);

            var project_url = api_host + "dashboard/avggraphs1/";
            user_selected = user_fn(users);
            if(user_selected !="all")
                ajax_data.users = user_selected;

            if(params_data.length > 0 ) {
                ajax_data.media_id = medias;

                var get_analytics = call_ajax_json("POST", project_url, JSON.stringify(ajax_data));
            if (get_analytics.length > 0 ) {
                var analytics_all = new Array();
                $("#chart_wrapper").show();
                for(let analytics of get_analytics) {
                    debugger;
                        var media_name = analytics.media_name;
                    for(var gender_index = 0;gender_index<analytics.data.length;gender_index++){
                        for(var age_index = 0;age_index<analytics.data[gender_index].age.length;age_index++){
                            if("AP" in analytics.data[gender_index].age[age_index].graph) {
                                var jsonString = JSON.stringify(analytics.data[gender_index].age[age_index].graph);
                                var age = analytics.data[gender_index].age[age_index].age;
                                var gender = analytics.data[gender_index].gender;
                                var numberWithCommas = function (x) {
                                    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                };
                                jsonString = jsonString.replace("attention", "Attention");
                                jsonString = jsonString.replace("AP", "Impact");
                                jsonString = jsonString.replace("ME", "Complexity");
                                jsonString = jsonString.replace("YY", "Valence");
                                jsonString = jsonString.replace("ET", "Arousal");
                                analytics_data = JSON.parse(jsonString);
                                analytics_data['media'] = media_name;
                                analytics_data['age'] = age;
                                analytics_data['gender'] = gender;
                                analytics_all.push(analytics_data);
                                analytics_data = new Array();
                            }
                        }
                    }

                    }


             if(analytics_all.length > 0)
             {
                 var params1 = 0;

                 for (var i = 0; i < graph_series.length; i++) {
                         graph_series.pop();
                 }

                 var params1 = 0;
                for(analytics_data of analytics_all) {
                    console.log(analytics_data);
                    var palette = new Rickshaw.Color.Palette();

                    for (var params = 0 ; params < params_mappers.length; params++) {
                        graph_series[params1] = {
                            "color": palette.color(),
                            "data": analytics_data[params_mappers[params]],
                            "name": analytics_data["media"] + ".Gender_" + analytics_data["gender"] +".Age_"+ analytics_data["age"] +"."+params_mappers[params]
                        };
                        params1++;
                    }
                }



                    var metrics = $(".metrics-wrapper").find(".selected_param").attr("val");
                    if (flag == 1) {
                        if(metrics == "avg"){
                            graph.render();
                        }
                        display_graph(json_data, params_data, metrics, params_mappers, medias);
                        flag = 2;//from second time onwards we need to update the graph
                    }
                    else {
                        graph.update();
                    }
                    $("#chart").find("h1").hide();

                }
                else{
                    graph.update();

                }
            }else{
                   // $("#chart_wrapper").hide();

                }
            }//end of params data if
            else{
                graph_series=[];
                graph.update();

               // $("#chart_wrapper").hide();
                //empty the table if the research data params is empty

            }
        }//end of media length greater than zero
        else{
            graph_series.pop();
            graph.update();
            // if the media dropdown does not have any media, we need to display message
        }

    } //end of populate graph

    //get api host url

    var api_host =$("#api_url").val();
    var host_url =$("#host_url").val();
    var project_list="";
    var project_dd = $(".project_list");
    var media_dd = $("#media_dd");
    var user_dd = $("#user_dd");
    var user_id = $("#user_id").val();
    var project_url = api_host + "dashboard/selectuserproject/";
    var json_data = {"user_id":user_id};
    var get_project_list = call_ajax("POST",project_url,json_data);
    if(get_project_list.length == 0){
        $(".project_list.dropdown_btn").html("No project list for this user");
    }
    else{
        get_project_list.reverse().forEach(function (data) {
            project_list += "<option id='project_"+data.project_id+"'>"+data.name+"</option>";
        });
        project_dd.append(project_list);
    }
    project_dd.on('change',function () {

        var project_id = $(this).children(":selected").attr("id").split("_")[1];
        var index = get_project_list.findIndex(function(x) {return x.project_id == project_id});


        var selected_project = get_project_list.filter(x => x.project_id == project_id);
        var media_in_project = selected_project[0].media;
        var media_list_filtered = media_in_project.filter(x => x.project_id == project_id);

        // hide media
        $("#image_wrapper_0,#image_wrapper_1").hide();
        var media_list = "";
      // var user_list = "<option id='user_all' selected>ALL</option>";
        var user_list = "";

        var unique_users_array = new Set();
        var default_media_flag = 0
        if(selected_project.length > 0 ){
            media_list_filtered.forEach(function (data) {
                if(data.name!=null) {
                    var selected_media = "";
                    if (default_media_flag == 0) {
                        selected_media = " selected"; //space is required
                    }

                    media_list += "<option id='" + data.media_id + "' value='" + data.media_id + "'  title='" + data.name + "'" +selected_media + ">" + data.name + "</option>";
                    data.testers.forEach(function (rec) {
                        if(rec.name!=null) {
                            if (!unique_users_array.has(rec.id)) {
                                console.log(rec.id);
                                user_list += "<option id='usertester_" + rec.id + "' value='usertester_" + rec.id + "'  title='" + rec.name + "' >" + rec.name + "</option>";
                                unique_users_array.add(rec.id);
                            }
                        }
                    });
                    default_media_flag = 1;
                }
            });
            media_dd.html("");
            user_dd.html("");
            if(media_list_filtered.length > 0){
                $("#graph_zone").show();
            }
            else{
                $("#graph_zone").hide();
            }
            media_dd.append(media_list).selectpicker('refresh');
            user_dd.append(user_list).selectpicker('refresh').selectpicker('selectAll');
            $("#image_wrapper_0").show();
            var mediaList = getSelectedMediaList();
            bind_video_image(media_dd,project_dd,get_project_list,mediaList);

        }else{
            media_dd.html("");
            user_dd.html("");
            media_dd.append(media_list).selectpicker('refresh');
            $(".image_wrapper").html("");
            user_dd.append(user_list).selectpicker('refresh');
            $("#graph_zone").hide();

        }
    });

    // on change of the media dropdown
    media_dd.on('change',function () {

        var mediaList = getSelectedMediaList();
        bind_video_image(media_dd,project_dd,get_project_list,mediaList);
    });
    user_dd.on('change',function () {

        var media_option_length = user_dd.find("option").length;
        if(media_option_length > 1) {
            populate_graph();
            populateSummaryGraph();
        }
    });

    $('#age_dd,#gender_dd').on('change',function () {
        populate_graph();
    });
    // to change the metrics avg, var
    $('div.metrics').on('click',function () {
        $(this).parent().parent().find(".selected_param").removeClass("selected_param");
        $(this).addClass("selected_param");
        var metrics = $(".metrics-wrapper").find(".selected_param").attr("val");
        if(metrics== "avg"){
            $("#chart_container,#chart,#y_axis,#x_axis,#preview,#smoother").show();
            $("#chart_distribution,#chart_distribution_container,#y_axis_distribution,#x_axis_distribution,#preview_distribution,#smoother_distribution").hide();
        }
        else{
            // if multiple research selected - select first parameter
            var $research_parameters = $(".research_parameters .selected_param")
            if($research_parameters.length > 1){
                $research_parameters.removeClass("selected_param");
                $(".research_parameters table tr td:first").addClass('selected_param');
            }
            $("#chart_container,#chart,#y_axis,#x_axis,#preview,#smoother").hide();
            $("#chart_distribution,#chart_distribution_container,#y_axis_distribution,#x_axis_distribution,#preview_distribution,#smoother_distribution").show();
            // load the chart .
          
            for(var media_index = 0;media_index < test_summary_data.length; media_index++){
                $("chart_distribution_error_label_"+media_index).hide();
                if("AP" in test_summary_data[media_index]["high_low"]){
                    stackBarTemplate(test_summary_data[media_index],media_index);
                }
                else{
                    // no data against the record
                    $("chart_distribution_error_label_"+media_index).show();
                }
            }

        }
        populate_graph();
    });

    var flag = 1; //for initial rendering
    var graph_series =  [];

    // instantiate our graph!
    var graph = new Rickshaw.Graph( {
        element: document.getElementById("chart"),
        renderer: 'line',
        width:(window.innerWidth/100)*65,
        height:(window.innerHeight/100)*50,
        stroke: true,
        preserve: true,
        series: graph_series
    });


    //$("#smoother>span").css({
    //    "left":"15%"
    //});
    //var smoother = new Rickshaw.Graph.Smoother({
     //   graph: graph,
     //   element: document.getElementById("smoother")
    //});
    //smoother.setScale(15);
   // graph.render();
    // on click of research parameters
    $(".research_parameters table tr td ").on("click",function () {
        // logic for toggle class
        var selected_research_param = $(".research_parameters .selected_param").length;
        var metrics = $(".metrics-wrapper").find(".selected_param").attr("val");
        var selected_research_param = $(".research_parameters .selected_param").text();
        if(metrics== "avg"){
            // minimum one research parameter should be selected
            if(selected_research_param != this.innerText){
                $(this).toggleClass("selected_param");
            }
        }
        else{
            $(this).parent().parent().find(".selected_param").removeClass("selected_param");
            $(this).addClass("selected_param");
            // if more than 1 research parameter reset all those
            for(var media_index = 0;media_index < test_summary_data.length; media_index++){
                $("chart_distribution_error_label_"+media_index).hide();
                if("AP" in test_summary_data[media_index]["high_low"]){
                    stackBarTemplate(test_summary_data[media_index],media_index);
                }
                else{
                    // no data against the record
                    $("chart_distribution_error_label_"+media_index).show();
                }
            }
        }
        //end of logic
        populate_graph();
    });




    function display_graph(json_data,params_data,metrics,params_mapper,medias) {
        json_data['media_id'] = medias;
        var ajax_data = {
            "select": params_data,
            "filter": json_data,
            "metrics": [metrics]
        };
        if (params_data.length > 0) {
                project_url = api_host + "dashboard/graphs/";
                var get_analytics = call_ajax("POST", project_url, JSON.stringify(ajax_data));
                var index = get_analytics.index;
        }
        var json_string_all = new Array();
        var analytics_all = new Array();
            if (params_data.length > 0) {
                if (Object.keys(get_analytics).length > 0) {
                    for (let media_seleted of medias) {
                        result = get_analytics.result[index[media_seleted]];

                        var media_name = result['media'];

                        if (result.graph.length > 0) {

                            for(let graph of result.graph) {
                                var jsonString = JSON.stringify(graph);



                                jsonString = jsonString.replace("attention", "Attention");
                                jsonString = jsonString.replace("AP", "Impact");
                                jsonString = jsonString.replace("ME", "Complexity");
                                jsonString = jsonString.replace("YY", "Valence");
                                jsonString = jsonString.replace("ET", "Arousal");
                                jsonString = jsonString.replace("AL", "Alertness");
                                jsonString = jsonString.replace("meditation", "Meditation");
                                jsonString = jsonString.replace("YF", "Familiarity");
                                jsonString = jsonString.replace("CR", "Creativity");
                                jsonString = jsonString.replace("CP", "Preparedness");
                                var analytics_data = JSON.parse(jsonString);
                                analytics_data['media'] = media_name;
                                analytics_all.push(analytics_data);
                            }
                        }
                    }

                        var params1 = 0;
                        for (var analytics_data of analytics_all) {
                            var palette = new Rickshaw.Color.Palette();

                            for (var params = 0; params < params_mapper.length; params++) {
                                /* for(var time=0;time < analytics_data[params_data[params]].length;time++) {
                                 analytics_data[params_data[params]][time]['x']
                                 = Date.parse(seconds_to_time(analytics_data[params_data[params]][time]["x"]));
                                 }*/
                                var metrics = $(".metrics-wrapper").find(".selected_param").attr("val");
                                if(metrics== "avg"){
                                graph_series[params1] = {
                                    "color": palette.color(),
                                    "data": analytics_data[params_mapper[params]],
                                    "name": analytics_data["media"] + ".Gender_" + analytics_data["gender"]+".Age_"+
                                    analytics_data["age"]+ "." + params_mapper[params]
                                };}

                                params1++;
                            }

                        }
                            if (flag == 1) {
                                //console.log(analytics_data[params_mapper[params]]);
                                graph.render();
                                //graph_distrubution.render()
                                flag = 2;//from second time onwards we need to update the graph
                            }
                            else {
                               // graph_distrubution.upadte()
                                graph.update();
                            }
                            $("#chart").find("h1").hide();
                        }
                        else {
                            $("#chart").prepend("<h1>No Graph Data exist</h1>");
                        }
                    }
        var line_graph_element_list = {
            smoother:"smoother",
            preview:"preview",
            yaxis:"y_axis",
            xaxis:"x_axis"
                
        }
        modify_graph(graph,line_graph_element_list);

    }//end of display_graph function
    var url_split = window.location.pathname.split('/');
    if (url_split.length > 3) {
        project_id = url_split[3];
        media_id=url_split[4];
        user_id=url_split[5];
        project_dd.find("#project_"+project_id).attr('selected','selected');
     }

    //by default to trigger all at a time Need to refactor this code...
    var project_id = project_dd.find("option:selected").attr("id").split("_")[1];
    var selected_project = get_project_list.filter(x => x.project_id == project_id);
    var media_in_project = selected_project[0].media;
    var media_list_filtered = media_in_project.filter(x => x.project_id == project_id);
    var index = get_project_list.findIndex(function(x) { return x.project_id == project_id  });
    var media_list = "";
    var user_list = "";

   // var user_list = "<option id='user_all' selected>ALL</option>";

    var sflag = 0;
    var unique_users = new Set();
    if(selected_project.length > 0 ){
        sflag = 0;
        media_list_filtered.forEach(function (data) {
            if(data.name!=null) {
                var selected_media = "";
                if (sflag == 0) {
                    selected_media = " selected"; //space is required
                }

                media_list += "<option id='" + data.media_id + "' value='" + data.media_id + "'  title='" + data.name + "'" +selected_media + ">" + data.name + "</option>";

                    data.testers.forEach(function (rec) {
                    if(rec.name!=null) {
                        if (!unique_users.has(rec.id)) {
                            user_list += "<option id='usertester_" + rec.id + "' value='usertester_" + rec.id + "' title='" + rec.name + "'>" + rec.name + "</option>";
                            unique_users.add(rec.id);
                        }
                    }
                });
                sflag = 1;
            }
        });
     }

    media_dd.html("");
    user_dd.html("");
    media_dd.append(media_list).selectpicker('refresh');
    user_dd.append(user_list).selectpicker('refresh').selectpicker('selectAll');


    var media_option_length = media_dd.find("option").length;
    if(media_option_length > 0) {
        //$('#media_dd select option:first-child').prop("selected", true);

        var mediaList = getSelectedMediaList();
        bind_video_image(media_dd,project_dd,get_project_list,mediaList)


    }

    $(".dashboard_image_wrapper").on("error", function(){
        var host_url =$("#host_url").val();

        $(this).attr('src', host_url+'static/img/dashboard/activity_type/25.jpg');
    });

    $(".reset_btn").on('click',function (event) {
        event.preventDefault();
        $(".ui-slider-range,#preview .ui-slider-handle:first,#preview_disribution .ui-slider-handle:first,#chart .detail").css({
           "left":"0"
        });
        var smoother = new Rickshaw.Graph.Smoother({
            graph: graph,
            element: document.getElementById("smoother")
        });
        smoother.setScale(0);
        var preview = new Rickshaw.Graph.RangeSlider({
            graph: graph,
            element: document.getElementById(elementlist.preview),
            values:[0,100]
        });
        var y_axis = new Rickshaw.Graph.Axis.Y({
            graph: graph,
            orientation: 'left',
            tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
            element: document.getElementById('y_axis')
        });
        y_axis.render();

    });

    $('[data-toggle="tooltip"]').tooltip();

    function populateSummaryGraph() {
        $('[data-toggle="tooltip"]').tooltip({tooltipClass: "tooltiptext"});

        $("#accordion_media_0,#accordion_media_1").accordion({
            collapsible: false,
            autoHeight: false
        });


        // Return with commas in between
        var numberWithCommas = function (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        };

        var test_summary_url = api_host + "dashboard/avggraphs1/";


        users =  Array.from($('#user_dd').find("option:selected"));
        var user_fn = users => users.map(rec => rec['id'].split("_")[1]);
        user_selected = user_fn(users);
        ajax_data = {
            "media_id":getSelectedMediaList()

        }
        if(user_selected.length > 0)
            ajax_data.users =  user_selected;



        test_summary_data = call_ajax_json("POST", test_summary_url,JSON.stringify(ajax_data));

        $("#accordion_media_0,#accordion_media_1").hide();
        var project_id = project_dd.children(":selected").attr("id").split("_")[1];
        var selected_project = get_project_list.filter(x => x.project_id == project_id);

        var media_in_project = selected_project[0].media;
        var media_list_filtered = media_in_project.filter(x => x.project_id == project_id);

        $('#chart_distribution_label_0,#chart_distribution_label_1').hide().text("");
        $("#chart_distribution_0,#chart_distribution_1").hide();
        for(var media_index = 0;media_index < test_summary_data.length; media_index++){
            if(test_summary_data.length == 1){
                $(' #content').css("padding-top", "24px");
            }else{
                $(' #content').css("padding-top", "3px");
            }
            $("#accordion_media_"+media_index).show();
            var media_name = media_list_filtered.filter(x => x.media_id == test_summary_data[media_index].media_id)[0].name;
            $(".accordion_media_"+media_index).text(media_name);
            $("#test_summary_wrapper_"+media_index).html("");
            $("#accordion_media_"+media_index).show();

            $('#chart_distribution_label_'+media_index).show().text(media_name);

            var media_id = test_summary_data[media_index].media_id;
            var attention = Math.round(test_summary_data[media_index].averages["attention"]);
            var complexity = Math.round(test_summary_data[media_index].averages["ME"]);
            var arousal = Math.round(test_summary_data[media_index].averages["ET"]);
            var impact = Math.round(test_summary_data[media_index].averages["AP"]);
            if(attention==0 && complexity==0 && arousal ==0 && impact == 0){
                attention = "-";
                complexity = "-";
                arousal = "-";
                impact = "-";
            }
            if(test_summary_data[media_index].averages["attention"] != undefined){

                     var datajson = {
                         class:"attention_"+media_index,
                         id:"Attention",
                         value:attention,
                         title:"A cognitive variable that measures the users focus while watching a video, advertisement or listening to an audio. It provides an overview of how immersive the user is during this period",
                         sparkline_id:"attention_"+media_index
                     }
                     $("#test_summary_wrapper_"+media_index).append(build_widget(datajson))
                     var param = {
                         id: "attention_"+media_index+"_sparkline_line",
                         data: Object.values(test_summary_data[media_index].range_averages["attention"]),
                         tooltipChartTitle:"Attention"
                     }
                     drawSparkLine(param);
                 }
                if(test_summary_data[media_index].averages["ME"] != undefined){
                        var datajson = {
                            class:"complexity_"+media_index,
                            id:"Complexity",
                            value:complexity,
                            title:"A key indicator of how a user's brain is processing the video, advertisement or audio. Higher the effort put in by the user to understand the content, greater the complexity and vice-versa",
                            sparkline_id:"complexity_"+media_index
                        }
                        $("#test_summary_wrapper_"+media_index).append(build_widget(datajson))
                    var param = {
                        id: "complexity_"+media_index+"_sparkline_line",
                        data: Object.values(test_summary_data[media_index].range_averages["ME"]),
                        tooltipChartTitle:"Complexity"
                    }
                    drawSparkLine(param);
                    }


                if(test_summary_data[media_index].averages["ET"] != undefined){
                    var datajson = {
                        class:"arousal_"+media_index,
                        id:"Arousal",
                        value:arousal,
                        title:"A variable that ranges from tranquility or boredom to alertness and excitement. It corresponds to similar concepts such as level of activation and stimulation",
                        sparkline_id:"arousal_"+media_index
                    }
                    $("#test_summary_wrapper_"+media_index).append(build_widget(datajson))
                    var param = {
                        id: "arousal_"+media_index+"_sparkline_line",
                        data: Object.values(test_summary_data[media_index].range_averages["ET"]),
                        tooltipChartTitle:"Arousal"
                    }
                    drawSparkLine(param);
                }
                if(test_summary_data[media_index].averages["AP"] != undefined){
                    var datajson = {
                        class:"impact_"+media_index,
                        id:"Impact",
                        value:impact,
                        sparkline_id:"impact_"+media_index,
                        title:"A moment by moment analysis of how much of positive or negative inclination is displayed by user during the course of watching the video, advertisement or audio"
                    }
                    $("#test_summary_wrapper_"+media_index).append(build_widget(datajson))
                    var param = {
                        id: "impact_"+media_index+"_sparkline_line",
                        data: Object.values(test_summary_data[media_index].range_averages["AP"]),
                        tooltipChartTitle:"Impact",

                    }
                    drawSparkLine(param);
                }

            $('[data-toggle="tooltip"]').tooltip();


         }
        var radarBackgroundColor = ["rgba(228, 164, 199,.5)","rgba(228, 164, 199,.5)"];
        var radarborderColor= ["#e4a4c7","#26759b"];

            // ==== Radar Chart ===
        for(var record_index=0;record_index < test_summary_data.length;record_index++){
            var radar_chart_data = test_summary_data[record_index].radar_chart;

            if(radar_chart_data.hasOwnProperty("Happy")){
                $("#emotion_graph_"+record_index).show();

                if(chart_object_ref.radar.length == record_index ){

                }else{
                    chart_object_ref.radar[record_index].chart.destroy();
                }
                   var radar_ctx = document.getElementById('radar-chart_'+record_index).getContext('2d');
                    var radar_chart = new Chart(radar_ctx, {
                        type: 'radar',
                        height:200,
                        // The data for our dataset
                        data: {
                            labels: ['Happy','Excited','Sad','Bored','Angry','Content'],
                            datasets: [{
                                data: [radar_chart_data["Happy"],radar_chart_data["Excited"],radar_chart_data["Sad"],
                                    radar_chart_data["Bored"],radar_chart_data["Angry"],radar_chart_data["Content"]],
                                fill:true,
                                backgroundColor:radarBackgroundColor[record_index],
                                borderColor:radarborderColor[record_index],
                                pointBackgroundColor:radarborderColor[record_index],
                                pointBorderColor:"#fff",
                                pointHoverBackgroundColor:"#fff",
                                pointHoverBorderColor:radarborderColor[record_index]}]
                        },
                        // Configuration options go here
                        options: {
                            legend: {
                                display: false
                            },
                        }
                    });

                    chart_object_ref.radar.push({"chart":radar_chart});

                /*}else{
                    console.log("Enter into Radar Seond Time"+record_index);
                    addData(chart_object_ref.radar[record_index].chart,datasets)
                }*/


                var media_name = test_summary_data[record_index].media_name;


                var dataPackHappy = [];
                var dataPackSad = [];
                var dataPackExcited = [];
                var dataPackBored = [];
                var dataPackContent = [];
                var dataPackAngry = [];
                var stack_bar_label = [];
                for(var stack_bar_index = 0;stack_bar_index<test_summary_data[record_index]["stacked_graph"].length;stack_bar_index++){
                    var stack_data = test_summary_data[record_index].stacked_graph[stack_bar_index];
                    dataPackHappy.push(stack_data["Happy"]);
                    dataPackSad.push(stack_data["Sad"]);
                    dataPackExcited.push(stack_data["Excited"]);
                    dataPackBored.push(stack_data["Bored"]);
                    dataPackContent.push(stack_data["Content"]);
                    dataPackAngry.push(stack_data["Angry"]);
                    stack_bar_label.push(stack_data["second"]);
                }
                var emotion_bar_datasets = [
                    {
                        label: 'Happy',
                        data: dataPackHappy,
                        backgroundColor: "#da1d83",
                        hoverBackgroundColor: "#da1d83",
                        hoverBorderWidth: 2,
                        hoverBorderColor: 'lightgrey'
                    },
                    {
                        label: 'Sad',
                        data: dataPackSad,
                        backgroundColor: "#26759b",
                        hoverBackgroundColor: "#26759b",
                        hoverBorderWidth: 2,
                        hoverBorderColor: 'lightgrey'
                    },
                    {
                        label: 'Bored',
                        data: dataPackBored,
                        backgroundColor: "#6ec53c",
                        hoverBackgroundColor: "#6ec53c",
                        hoverBorderWidth: 2,
                        hoverBorderColor: 'lightgrey'
                    },
                    {
                        label: 'Excited',
                        data: dataPackExcited,
                        backgroundColor: "#c2bb1b",
                        hoverBackgroundColor: "#c2bb1b",
                        hoverBorderWidth: 2,
                        hoverBorderColor: 'lightgrey'
                    },
                    {
                        label: 'Angry',
                        data: dataPackAngry,
                        backgroundColor: "#00bcd4",
                        hoverBackgroundColor: "#00bcd4",
                        hoverBorderWidth: 2,
                        hoverBorderColor: 'lightgrey'
                    },
                    {
                        label: 'Content',
                        data: dataPackContent,
                        backgroundColor: "#ca46cd",
                        hoverBackgroundColor: "#ca46cd",
                        hoverBorderWidth: 2,
                        hoverBorderColor: 'lightgrey'
                    }
                ]
                $("#chart_distribution_error_label_"+record_index).hide();

                if("AP" in test_summary_data[record_index]["high_low"]){
                    stackBarTemplate(test_summary_data[record_index],record_index);
                }
                else{
                    // no data against the record
                    $("#chart_distribution_error_label_"+media_index).show();
                }

                if(chart_object_ref.emotion.length == record_index ) {
                }else{
                    chart_object_ref.emotion[record_index].chart.destroy();
                }

                    var emotion_bar_ctx = document.getElementById('emotion_bar_chart_'+record_index).getContext('2d');
                    var chart = new Chart(emotion_bar_ctx, {
                            type: 'bar',
                            scaleOverride:true,
                            scaleSteps:10,
                            scaleStartValue:0,
                            scaleStepWidth:100,
                            height:200,
                            data: {
                                labels: stack_bar_label,
                                datasets: emotion_bar_datasets
                            },
                            options: {
                                tooltips: {
                                    mode: 'label',
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
                                        }
                                    }
                                },
                                scales: {
                                    xAxes: [{
                                        stacked: true,
                                        gridLines: {display: false},
                                        ticks:{
                                            stepSize : 20,

                                        }
                                    }],
                                    yAxes: [{
                                        maxTick:100,
                                        stacked: true,
                                        ticks: {
                                            callback: function (value) {
                                                return numberWithCommas(value);
                                            },
                                            max:100,
                                            min:0
                                        },
                                    }],
                                }, // scales
                                legend: {display: true}
                            } // options
                        }
                    );
                    chart_object_ref.emotion.push({"chart":chart})

                //}
                //else{
                  //  addData(chart_object_ref.emotion[record_index].chart,emotion_bar_datasets)
                //}
            }
            else{
               // Hide Emotion and Radar Graphs
                $("#emotion_graph_"+record_index).hide();
            }
        }
    }

    function bind_video_image(media_dd,project_dd,get_project_list,media_list){
        $(".emotion_graph_0,.emotion_graph_1").hide();
        var media_option_length =media_dd.find('option').length;
        $(".image_wrapper").hide()
        if(media_option_length > 0) {
            var project_id = project_dd.children(":selected").attr("id").split("_")[1];

            var selected_project = get_project_list.filter(x => x.project_id == project_id);

            var media_in_project = selected_project[0].media;
            var media_list_filtered = media_in_project.filter(x => x.project_id == project_id);

            for(var i=0;i<media_list.length;i++){
                // get media url
                var media_url = media_list_filtered.filter(x => x.media_id == media_list[i])[0].url;
                $("#image_wrapper_"+i+",.emotion_graph_"+i).show();
                var media_name = media_list_filtered.filter(x => x.media_id == media_list[i])[0].name;
                $("#selected_media_name_"+i).text("Emotional Spectrum :- "+media_name);
                $("#image_wrapper_"+i).html("").append("<label class='heading'>" + media_name+"</label>");
                if(media_url != "" && media_url != null){
                    $("#image_wrapper_"+i).append("<video id='homeScreenVideo' src='"+
                        media_url+ "' class='dashboard_image_wrapper' controls>")
                        //"onplay='onSeekingHomeScreenVideo()' onpause='stopCursor()' controls>")
                }else{
                    $("#image_wrapper_"+i).append("<img class='dashboard_image_wrapper'" +
                        " src='"+host_url+'static/img/dashboard/activity_type/25.jpg' +
                        " alt='Image not found'>");
                    $(".dashboard_image_wrapper").on("error", function () {
                        $(this).attr('src',host_url+ 'static/img/dashboard/activity_type/25.jpg');
                    });
                }

            }

             populate_graph();
              populateSummaryGraph();
        }
    }

    function drawSparkLine(param) {
        $("#" + param.id).sparkline(array_value_round(param.data), {
            type: 'line',
            width: '100%',
            height: '40',
            spotRadius: 0,
            lineWidth: 1,
            lineColor: '#ffffff',
            fillColor: false,
            minSpotColor: false,
            maxSpotColor: false,
            highlightLineColor: '#ffffff',
            highlightSpotColor: '#ffffff',
            tooltipChartTitle: param.tooltipChartTitle,
            tooltipPrefix: '',
            spotColor: '#ffffff',
            valueSpots: {
                '0:': '#ffffff'
            }
        });
    }
}); //end of document ready


// widget template
function build_widget(json){
    return '<div class="panel-wrapper">'+
                        '<div class="panel panel-info panel-colorful '+ json.class + '">' +
        '<div class="padding-top-10 text-center">' +
        '<p class="text-lg text-semibold"><span class="panel-heading">'+ json.id+'</span> ' +
         '<i class="fa fa-info-circle black-tooltip" aria-hidden="true" data-toggle="tooltip" title="'+json.title +'"></i>' +
         '</p>  <p class="mar-no"> <span class="text-bold">'+json.value+'</span> </p>'+
         '</div><div class="pad-all text-center"> <div id="'+json.class+'_sparkline_line">'+
        '<canvas width="170" height="40" style="display: inline-block; width: 170.703px; height: 40px; vertical-align: top;"></canvas>'+
        '</div> </div></div> </div>';
}

function array_value_round(input_array){
    var array = []
    for(i=0;i<input_array.length;i++){
        array.push(Math.round(input_array[i]));
    }
    return array;
}

function getSelectedMediaList() {
    var medias  = [];
    // if the media dropdown have any media, it will come here.
    medias = Array.from($('#media_dd').find("option:selected"));
    var media_fn = medias=> medias.map(rec => parseInt(rec['id']));
    medias = media_fn(medias);
    return medias;
}

function stackBarTemplate(data,media_index){
      $('#chart_distribution_'+media_index,"#chart_wrapper").show();
        var chart_distribution_ctx = document.getElementById('chart_distribution_' + media_index).getContext('2d');
        var dataPackLow = [];
        var dataPackMedium = [];
        var dataPackHigh = [];
        var dataPackVeryHigh = [];
        // selected Metrics value
        var selectedMetric ="attention";
        $(".research_parameters .selected_param").each(function(i, v){
            selectedMetric = $(this).attr("val");
        });


        var stack_bar_label = [];
        var hasDistributionProperty = "distribution" in data["high_low"][selectedMetric];
        if(hasDistributionProperty){
            var stackBarData = data["high_low"][selectedMetric]["distribution"];
            for(var stack_bar_index = 0;stack_bar_index<stackBarData.length;stack_bar_index++){
                var stack_data = stackBarData[stack_bar_index];
                dataPackLow.push(stack_data["low"]);
                dataPackMedium.push(stack_data["medium"]);
                dataPackHigh.push(stack_data["high"]);
                dataPackVeryHigh.push(stack_data["very high"]);
                stack_bar_label.push(stack_data["second"]);
            }
            // Return with commas in between
            var numberWithCommas = function (x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            };
            var dataSets = [
                {
                    label: 'Low',
                    data: dataPackLow,
                    backgroundColor: "#da1d83",
                    hoverBackgroundColor: "#da1d83",
                    hoverBorderWidth: 2,
                    hoverBorderColor: 'lightgrey'
                },
                {
                    label: 'Medium',
                    data: dataPackMedium,
                    backgroundColor: "#26759b",
                    hoverBackgroundColor: "#26759b",
                    hoverBorderWidth: 2,
                    hoverBorderColor: 'lightgrey'
                },
                {
                    label: 'High',
                    data: dataPackHigh,
                    backgroundColor: "#6ec53c",
                    hoverBackgroundColor: "#6ec53c",
                    hoverBorderWidth: 2,
                    hoverBorderColor: 'lightgrey'
                },
                {
                    label: 'Very High',
                    data: dataPackVeryHigh,
                    backgroundColor: "#c2bb1b",
                    hoverBackgroundColor: "#c2bb1b",
                    hoverBorderWidth: 2,
                    hoverBorderColor: 'lightgrey'
                }
            ];

            if(chart_object_ref.distribution.length == media_index ) {

            }else{
                chart_object_ref.distribution[media_index].chart.destroy();
            }
                console.log("stack_bar_chart"+media_index);
                console.log("stack_bar_chart_data"+JSON.stringify(dataSets));

            var stack_bar_chart = new Chart(chart_distribution_ctx, {
                    type: 'bar',
                    scaleOverride:true,
                    scaleSteps:20,
                    scaleStartValue:0,
                    scaleStepWidth:100,
                    height:100,
                    data: {
                        labels: stack_bar_label,
                        datasets: dataSets
                    },
                    options: {

                        tooltips: {
                            mode: 'label',
                            callbacks: {
                                label: function (tooltipItem, local_data) {
                                    return local_data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
                                }
                            }
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                                gridLines: {display: false},
                                ticks:{
                                    stepSize : 20,

                                }
                            }],
                            yAxes: [{
                                maxTick:100,
                                ticks:{
                                    stepSize : 20,

                                },
                                stacked: true,
                                ticks: {
                                    callback: function (value) {
                                        return numberWithCommas(value);
                                    },
                                    max:100,
                                    min:0
                                },
                            }],
                        }, // scales
                        legend: {display: true}
                    } // options
                });

                chart_object_ref.distribution.push({"chart": stack_bar_chart});


           // }
            /*else{
                addData(chart_object_ref.distribution[media_index].chart,dataSets)
            }*/
        }
}

function addData(chart, data) {

        chart.data.datasets.forEach(function(dataset) {
            chart.data.datasets.pop();
        });
        chart.data.datasets = data;
        chart.update();
    
}