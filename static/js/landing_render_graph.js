function modify_graph(graph) {
    // For displaying Y axis
    var y_axis = new Rickshaw.Graph.Axis.Y({
        graph: graph,
        orientation: 'left',
        tickValues:[0,10,20,30,40,50,60,70,80,90],
        tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
        element: document.getElementById('y_axis')
    });
    y_axis.render();
    var smoother = new Rickshaw.Graph.Smoother({
        graph: graph,
        element: document.getElementById(graph.smoother)
    });
    smoother.setScale(10);
    // For displaying X axis
    var graph_data_length = 20;
    if(graph.stackedData.length>0 && graph.stackedData[0].length > 100) graph_data_length = graph.stackedData[0].length/20;
    else if(graph.stackedData.length>0) graph_data_length = graph.stackedData[0].length;
    var xAxis = new Rickshaw.Graph.Axis.X({
        graph: graph,
        orientation: "bottom",
        ticks:graph_data_length,
        tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
        element: document.getElementById('x_axis')

    });

    xAxis.render();
    //Displays values on hover of the data
    var hover_detail = new Rickshaw.Graph.HoverDetail({
        graph: graph,
        xFormatter: function (x) {
            return new Date(x * 1000).toString();
        }
    });

}
