
var player,
    time_update_interval = 0;
var olderMediaList;
var media_id = "";
var test_summary_data;
var rickColorConfig = {
    "Attention":['#0594d3','#ff7a45'],
    "Engagement":['#3ecdf9','#ff954d'],
    "Activation":['#29ffef','#ffb966'],
    "Enjoyability":['#99fffa','#ffe299']
}
var api_host = $("#api_url").val();

// To download test media
function download_test(media_id)
{
    var media_data_url = api_host + "dashboard/download_test/";
    var download_test_data = call_ajax_text("POST", media_data_url, {media_id: media_id});
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(download_test_data);
    // hiddenElement.target = '_blank';
    hiddenElement.download = 'data_test_'+media_id+'.csv';
    hiddenElement.click();
    // var uri = 'data:application/csv;charset=UTF-8,'+encodeURIComponent(download_test_data);
    // window.open(uri,'data.csv');
    return false;
}


function onSeekingHomeScreenVideo(){
    var detail_cache_obj = $(".detail");
    detail_cache_obj.addClass("active");
    detail_cache_obj.append("<div class='x_label' ><label id='minutes'>00</label>:<label id='seconds'>00</label></div>");
    detail_cache_obj.append("<div class='item active left' id='x_label' style='top: 0.0px;'>Time:</div>");
    $(".item").append("<div class='dot active' style='top: 242.382px; border-color: rgb(47, 37, 74);top: 10px; left:-8px;'></div>");

    try {
        var videoLength = document.getElementById("homeScreenVideo").duration;
        var counter = 1;
        var currentTime = 0;

        width = $("#chart").width();
        var timer = setInterval(function () {
            detail_cache_obj.css("left", counter);


            $("#homeScreenVideo").on(
                "timeupdate",
                function (event) {
                    counter = Math.round((Math.round(this.currentTime) / Math.round(parseFloat(this.duration))) * width) - 1;
                    detail_cache_obj.append("<div class='item active left' id='x_label' style='top: 0.0px;'>Time:</div>");
                    document.getElementById("x_label").textContent = "Time : " + Math.round(this.currentTime);
                    currentTime++;
                    if (this.currentTime == this.duration) {
                        clearInterval(timer);
                    }

                });
        }, Infinity);
        var minutesLabel = document.getElementById("minutes");
        var secondsLabel = document.getElementById("seconds");
        var totalSeconds = 0;
        setInterval(setTime, 1000);
    }
    catch(err)
    {clearInterval(timer);}
    function setTime()
    {
        ++totalSeconds;
        secondsLabel.innerHTML = pad(totalSeconds%60);
        minutesLabel.innerHTML = pad(parseInt(totalSeconds/60));
    }

    function pad(val)
    {
        return val + "";

    }
}

// var timer = setInterval(function () {
//     var counter = 5;
//     var currentTime = 0;
//     try {
//         var videoLength = document.getElementById("homeScreenVideo").duration;
//  // $(".detail").css("left",counter);
//     counter =  counter + 14;
//     currentTime ++;
//     if(currentTime == videoLength.toFixed(0)){
//         clearInterval(timer);
//     }
//     }
//     catch(err)
//     {clearInterval(timer);}
//
// },Infinity);
var chart_object_ref =  {
    emotion:[],
    less_emotion:[],
    radar:[],
    radar_less_emotion:[],
    distribution:[],
    averageGraph:[],
    isUserDropDownInit:false,
    isGraphFirstTimeInit:true,
    graphDefaultData:[]
};
function stopCursor() {
    //$(".detail").addClass("inactive")
    clearInterval(timer);
    // onSeekingHomeScreenVideo().stop();
}

function initialize(){
    // disable current this
    return false;
    // Update the controls on load
    updateTimerDisplay();
    updateProgressBar();

    // Clear any old interval.
    clearInterval(time_update_interval);

    // Start interval to update elapsed time display and
    // the elapsed part of the progress bar every second.
    time_update_interval = setInterval(function () {
        updateTimerDisplay();
        updateProgressBar();
    }, 1000);
    $('#volume-input').val(Math.round(player.getVolume()));
}


// This function is called by initialize()
function updateTimerDisplay(){
    // Update current time text display.
    $('#current-time').text(formatTime( player.getCurrentTime() ));
    $('#duration').text(formatTime( player.getDuration() ));
}


// This function is called by initialize()
function updateProgressBar(){
    // Update the value of our progress bar accordingly.
    $('#progress-bar').val((player.getCurrentTime() / player.getDuration()) * 100);
}


// Progress bar

$('#progress-bar').on('mouseup touchend', function (e) {

    // Calculate the new time for the video.
    // new time in seconds = total duration in seconds * ( value of range input / 100 )
    var newTime = player.getDuration() * (e.target.value / 100);

    // Skip video to new time.
    player.seekTo(newTime);

});


// Playback

$('#play').on('click', function () {
    player.playVideo();
    // ("#chart").addClass('.rickshaw_graph .detail');
});


$('#pause').on('click', function () {
    player.pauseVideo();
});


// Sound volume


$('#mute-toggle').on('click', function() {
    var mute_toggle = $(this);

    if(player.isMuted()){
        player.unMute();
        mute_toggle.text('volume_up');
    }
    else{
        player.mute();
        mute_toggle.text('volume_off');
    }
});

$('#volume-input').on('change', function () {
    player.setVolume($(this).val());
});


// Other options

$('#speed').on('change', function () {
    player.setPlaybackRate($(this).val());
});

$('#quality').on('change', function () {
    player.setPlaybackQuality($(this).val());
});


// Playlist

$('#next').on('click', function () {
    player.nextVideo()
});

$('#prev').on('click', function () {
    player.previousVideo()
});


// Load video

$('.thumbnail').on('click', function () {

    var url = $(this).attr('data-video-id');
    player.cueVideoById(url);

});


// Helper Functions

function formatTime(time){
    time = Math.round(time);
    var minutes = Math.floor(time / 60),
        seconds = time - minutes * 60;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    return minutes + ":" + seconds;
}


$('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
});

$(function () {
    $('.nav li a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
    $(this).removeClass('active');
    // hide 6 spectrum chart for other users
    var user_id = $("#user_id").val()
    if(user_id != 53){
        $("#radar-chart_1,#emotion_bar_chart_1,"+
        "#emotion_bar_chart_0,#radar-chart_0").parent().hide()

    }
    
$("a.filter_title").on('click',function(){
    if($(this).hasClass("collapsed"))
    {$(".filter_icon").removeClass("ti-angle-double-down").addClass("ti-angle-double-up");}
    else
    {$(".filter_icon").removeClass("ti-angle-double-up").addClass("ti-angle-double-down");}
});


    /*if($(location).attr('pathname')== "/users/dashboard/"){
        var selected =$("#dashboard");
        selected.addClass("selected_url");

    }*/
    if(/dashboard/.test(self.location.href)){
        var selected =$(".sidebar-wrapper ul.nav");
        selected.find("li.active").removeClass("active");
        selected.find("li:nth-child(2)").addClass("active");
    }      
    function populate_graph() {
        $("#chart_wrapper").show();

        //get selected params, metrics, age, gender filters
        var params_data = [];

        var params_mappers = [];
        var gender_filter = "";
        $(".research_parameters .selected_param").each(function(i, v){
            params_data[i] = $(this).attr("val");
            params_mappers[i] = $(this).text();
        });
        var metrics = $(".metrics-wrapper").find(".selected_param").attr("val");
        var age_val = $("#age_dd").children(":selected").attr("val");
        var gender_val =  $("#gender_dd").children(":selected").attr("val");

        switch(gender_val){
            case "all": gender_filter = "all"; break;
            case "versus": gender_filter = "versus";break;
            case "male":gender_filter = ["male"];break;
            case "female":gender_filter = ["female"];break;
            default:gender_filter = "all";break;
        }

        if(age_val !="all" && age_val != "versus"){
            age_val = [parseInt(age_val)];
        }
        var ajax_data = {
            "age":age_val,
            "gender":gender_filter,
            "media_id":media_id

        };
        //end of setting the json data
        if(media_dd.find("option").length > 0)
        {
            // if the media dropdown have any media, it will come here.
            medias = Array.from($('#media_dd').find("option:selected"));
            users =  Array.from($('#user_dd').find("option:selected"));
            var media_fn = medias => medias.map(rec => parseInt(rec['id']));
            var user_fn = users => users.map(rec => rec['id'].split("_")[1]);
            medias=media_fn(medias);

            var project_url = api_host + "dashboard/avggraphs1/";
            user_selected = user_fn(users);
            if(user_selected !="all")
                ajax_data.users = user_selected;
            if(params_data.length > 0 ) {
                ajax_data.media_id = medias;
                var get_analytics = chart_object_ref.graphDefaultData;
                if(!chart_object_ref.isGraphFirstTimeInit) {
                    get_analytics = call_ajax_json("POST", project_url, JSON.stringify(ajax_data));

                }
                // if graph is second time load then fetch from database
                chart_object_ref.isGraphFirstTimeInit = false;

                if (get_analytics.length > 0 ) {
                    var analytics_all = new Array();
                    $("#chart_wrapper").show();
                    for(let analytics of get_analytics) {
                        var media_name = analytics.media_name;
                        for(var gender_index = 0;gender_index<analytics.data.length;gender_index++){
                            for(var age_index = 0;age_index<analytics.data[gender_index].age.length;age_index++){
                                if("AP" in analytics.data[gender_index].age[age_index].graph) {
                                    var jsonString = JSON.stringify(analytics.data[gender_index].age[age_index].graph);
                                    var age = analytics.data[gender_index].age[age_index].age;
                                    var gender = analytics.data[gender_index].gender;
                                    var numberWithCommas = function (x) {
                                        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    };
                                    jsonString = jsonString.replace("attention", "Attention");
                                    jsonString = jsonString.replace("AP", "Enjoyability");
                                    jsonString = jsonString.replace("ME", "Engagement");
                                    jsonString = jsonString.replace("YY", "Valence");
                                    jsonString = jsonString.replace("ET", "Activation");
                                    analytics_data = JSON.parse(jsonString);
                                    analytics_data['media'] = media_name;
                                    analytics_data['age'] = age;
                                    analytics_data['gender'] = gender;
                                    analytics_all.push(analytics_data);
                                    analytics_data = new Array();
                                }
                            }
                        }

                    }
                    $('#chart_container_error_label').hide();
                    $("#chart_container").show();


                    if(analytics_all.length > 0)
                    {
                        var params1 = 0;

                        while(graph_series != 0)
                            graph_series.pop();


                        var params1 = 0;
                        var colorCounter = 0
                        for(analytics_data of analytics_all) {
                            
                            for (var params = 0 ; params < params_mappers.length; params++) {
                               graph_series[params1] = {
                                    "color":rickColorConfig[params_mappers[params]][colorCounter],
                                    "data": analytics_data[params_mappers[params]],
                                    "name": analytics_data["media"] + ".Gender_" + analytics_data["gender"] +".Age_"+ analytics_data["age"] +"."+params_mappers[params]
                                };
                                params1++;

                            }
                            colorCounter++;
                        }



                        if (flag == 1) {

                            graph.render();
                            flag = 2;//from second time onwards we need to update the graph
                        }
                        else {
                            graph.update();
                        }
                        $("#chart").find("h1").hide();

                    }
                    else{
                        $('#chart_container_error_label').show();
                        $("#chart_container").hide();
                    }
                }else{
                    $('#chart_container_error_label').show();
                    $("#chart_container").hide();

                }
            }//end of params data if
            else{
                while(graph_series.length != 0)
                    graph_series.pop();
                graph.update();

                // $("#chart_wrapper").hide();
                //empty the table if the research data params is empty

            }
        }//end of media length greater than zero
        else{
            noDataFound(media_dd.find("option").length);
            while(graph_series.length != 0)
                graph_series.pop();
            graph.update();
            // if the media dropdown does not have any media, we need to display message
        }
        if(chart_object_ref.averageGraph.length == 0){
            chart_object_ref.averageGraph.push({'Init':true});
            var line_graph_element_list = {
                smoother:"smoother",
                preview:"preview",
                yaxis:"y_axis",
                xaxis:"x_axis"

            }
            modify_graph(graph,line_graph_element_list);
        }
    } //end of populate graph

    //get api host url
    var url_split = window.location.pathname.split('/');
    var project_id = ""
    if (url_split.length > 4) {
        project_id = url_split[3];
        media_id=url_split[4];
        user_id=url_split[5];
    }
    var api_host =$("#api_url").val();
    var host_url =$("#host_url").val();
    var project_list="";
    var project_dd = $("#project_dd");
    var media_dd = $("#media_dd");
    var user_dd = $("#user_dd");
    var user_id = $("#user_id").val();
    var project_url = api_host + "dashboard/selectuserproject/";
    var json_data = {"user_id":user_id};
    var get_project_list = call_ajax("POST",project_url,json_data);
    if(get_project_list.length == 0){
        $(".project_list.dropdown_btn").html("No project list for this user");
        // show no data message in left side dashboard
        noDataFound(get_project_list.length);
    }
    else{
        var isfirstProject = true;
        get_project_list.reverse().forEach(function (data) {
            var selected = "";
            if(project_id == "" && isfirstProject || project_id==data.project_id)
                selected = "selected='selected' "

            project_list += "<option id='project_"+data.project_id+"' title='"+data.name+"' "+selected+">"+data.name+"</option>";
            isfirstProject = false;
        });
        project_dd.append(project_list).selectpicker('refresh');
    }

    // on Changing the Project dropdown
    project_dd.on('change',function (event) {
        event.preventDefault();
        // debugger;
        // $(".main-panel .container-fluid").addClass("dashboard_overlay");
        // pick out project id 
        var project_id = $(this).children(":selected").attr("id").split("_")[1];
        // selected index of the project in the dropdown
        var index = get_project_list.findIndex(function(x) {return x.project_id == project_id});


        var selected_project = get_project_list.filter(x => x.project_id == project_id);
        var media_in_project = selected_project[0].media;
        var media_list_filtered = media_in_project.filter(x => x.project_id == project_id);

        // hide media
        $("#image_wrapper_0,#image_wrapper_1").hide();
        var media_list = "";
        // var user_list = "<option id='user_all' selected>ALL</option>";
        var user_list = "";

        var unique_users_array = new Set();
        var default_media_flag = 0;
        if(selected_project.length > 0 ){
            noDataFound(media_list_filtered.length);

            media_list_filtered.forEach(function (data) {
                if(data.name!=null) {
                    var selected_media = "";
                    if (default_media_flag == 0 && data.testers.length != 0) {
                        selected_media = " selected"; //space is required
                        data.testers.forEach(function (rec) {
                            if(rec.name!=null) {
                                if (!unique_users_array.has(rec.id)) {
                                    user_list += "<option id='usertester_" + rec.id + "' value='usertester_" + rec.id + "'  title='" + rec.name + "' >" + rec.name + "</option>";
                                    unique_users_array.add(rec.id);
                                }
                            }
                        });
                    }

                    var disable = "";
                    if(data.testers.length == 0)
                        disable = " disabled style='background: #bd4734; color: #fff;border-bottom:#ece8e8 1px solid' ";
                    else
                        default_media_flag = 1;

                    media_list += "<option  id='" + data.media_id + "' value='" + data.media_id + "'  title='" + data.name + "'" +selected_media + disable  + ">" + data.name + "</option>";

                    noDataFound(data.testers.length);

                }
            });
            media_dd.html("");
            user_dd.html("");

            media_dd.append(media_list).selectpicker('refresh');
            user_dd.append(user_list).selectpicker('refresh').selectpicker('selectAll');
            $("#image_wrapper_0").show();
            var mediaList = getSelectedMediaList();
            bind_video_image(media_dd,project_dd,get_project_list,mediaList);

        }else{
            media_dd.html("");
            user_dd.html("");
            media_dd.append(media_list).selectpicker('refresh');
            $(".image_wrapper").html("");
            user_dd.append(user_list).selectpicker('refresh');
            $("#graph_zone").hide();

        }
        // $(".main-panel .container-fluid").removeClass("dashboard_overlay");
    });

    // on change of the media dropdown
    media_dd.on('change',function (event) {
        event.preventDefault();
        // $(".main-panel .container-fluid").addClass("dashboard_overlay");
        var mediaList = getSelectedMediaList();
        noDataFound(mediaList.length);
        user_dd.html("").selectpicker('refresh');
        if(mediaList.length != 0)
            user_dd.append(user_list).selectpicker('refresh').selectpicker('selectAll');
        noDataFound(user_list.length);
        bind_video_image(media_dd,project_dd,get_project_list,mediaList);
        // $(".main-panel .container-fluid").removeClass("dashboard_overlay");
    });
    user_dd.on('change',function (event) {
        event.preventDefault();
        // $(".main-panel .container-fluid").addClass("dashboard_overlay");
        var users =  Array.from($('#user_dd').find("option:selected"));
        var user_fn = users => users.map(rec => rec['id'].split("_")[1]);
        user_selected = user_fn(users);
        noDataFound(user_selected.length);

        var mediaList = getSelectedMediaList();
        var selected_project = get_project_list.filter(x => x.project_id == project_id);
        var media_in_project = selected_project[0].media;
        var media_list_filtered = media_in_project.filter(x => x.project_id == project_id);

        for(var i=0;i<mediaList.length;i++){
            var media_data = media_list_filtered.filter(x => x.media_id == mediaList[i])[0];
            if(media_data != undefined && media_data.activitytype_id == 3) {

                if (media_data.testers.length > 0) {
                        populate_screen_video(i,media_data.media_id,user_selected[0])

                }

            }
        }
        if(chart_object_ref.isUserDropDownInit){
            var media_option_length = user_dd.find("option").length;
            if(media_option_length > 1) {
                populate_graph();
                populateSummaryGraph();
            }
        }

        chart_object_ref.isUserDropDownInit = true;
    });

    $('#age_dd,#gender_dd').on('change',function (event) {
        event.preventDefault();
        populate_graph();
    });
    // to change the metrics avg, var
    $('div.metrics').on('click',function (event) {
        event.preventDefault();
        $(this).parent().parent().find(".selected_param").removeClass("selected_param");
        $(this).addClass("selected_param");
        var metrics = $(".metrics-wrapper").find(".selected_param").attr("val");
        if(metrics== "avg"){
            $("#chart_container,#chart,#y_axis,#x_axis,#preview,#smoother").show();
            $("#chart").next().show();
            $("#chart_distribution,#chart_distribution_container,#preview_distribution,#smoother_distribution").hide();
        }
        else{
            // if multiple research selected - select first parameter
            var $research_parameters = $(".research_parameters .selected_param")
            if($research_parameters.length > 1){
                $research_parameters.removeClass("selected_param");
                $(".research_parameters table tr td:first").addClass('selected_param');
            }
            $("#chart_container,#chart,#y_axis,#x_axis,#preview,#smoother").hide();
            $("#chart").next().hide();
            $("#chart_distribution,#chart_distribution_container,#preview_distribution,#smoother_distribution").show();
            // load the chart .

            for(var media_index = 0;media_index < test_summary_data.length; media_index++){
                $("#chart_distribution_error_label_"+media_index).hide();
                $("#chart_distribution_"+media_index).show();
                if("AP" in test_summary_data[media_index]["high_low"]){
                    stackBarTemplate(test_summary_data[media_index],media_index);
                }
                else{
                    // no data against the record
                    $("#chart_distribution_"+media_index).hide();
                    $("#chart_distribution_error_label_"+media_index).show();
                }
            }

        }
        populate_graph();
    });

    var flag = 1; //for initial rendering
    var graph_series =  [];
    var height = $('.left_pane').height();
    // instantiate our graph!
    var graph = new Rickshaw.Graph( {
        element: document.getElementById("chart"),
        renderer: 'line',
        width:(window.innerWidth/100)*65,
        height:height-200,
        stroke: true,
        preserve: true,
        series: graph_series
    });

    // on click of research parameters
    $(".research_parameters table tr td ").on("click",function () {
        // logic for toggle class
        var selected_research_param = $(".research_parameters .selected_param").length;
        var metrics = $(".metrics-wrapper").find(".selected_param").attr("val");
        var selected_research_param = $(".research_parameters .selected_param").text();
        switch($(this).attr("val")){
            case "attention": $(this).toggleClass("at_param");break;
            case "ME": $(this).toggleClass("eng_param");break;
            case "ET": $(this).toggleClass("act_param");break;
            case "AP": $(this).toggleClass("enj_param");break;
        }
        if(metrics== "avg"){
            // minimum one research parameter should be selected
            if(selected_research_param != this.innerText){
                $(this).toggleClass("selected_param");
            }
        }
        else{
            $(this).parent().parent().find(".selected_param").removeClass("selected_param");
            $(this).addClass("selected_param");
            // if more than 1 research parameter reset all those
            for(var media_index = 0;media_index < test_summary_data.length; media_index++){
                $("#chart_distribution_error_label_"+media_index).hide();
                $("#chart_distribution_"+media_index).show();

                if("AP" in test_summary_data[media_index]["high_low"]){
                    stackBarTemplate(test_summary_data[media_index],media_index);
                }
                else{
                    // no data against the record
                    $("#chart_distribution_error_label_"+media_index).show();
                    $("#chart_distribution_"+media_index).hide();

                }
            }
        }
        //end of logic
        populate_graph();
    });


    //by default to trigger all at a time Need to refactor this code...
    var project_id = project_dd.find("option:selected").attr("id").split("_")[1];
    var selected_project = get_project_list.filter(x => x.project_id == project_id);
    var media_in_project = selected_project[0].media;
    var media_list_filtered = media_in_project.filter(x => x.project_id == project_id);
    var index = get_project_list.findIndex(function(x) { return x.project_id == project_id  });
    var media_list = "";
    var user_list = "";

    // var user_list = "<option id='user_all' selected>ALL</option>";
    var sflag = 0;
    var unique_users = new Set();
    if(selected_project.length > 0 ){

        noDataFound(media_list_filtered.length);

        media_list_filtered.forEach(function (data) {
            if(data.name!=null) {
                var selected_media = "";
                if ((sflag == 0 && data.testers.length != 0 && (media_id == "" || media_id == undefined ))|| (media_id == data.media_id)  ) {
                    selected_media = " selected"; //space is required
                }
                var disable = "";
                if(data.testers.length == 0)
                    disable = " disabled style='background: #bd4734; color: #fff;border-bottom:#ece8e8 1px solid' ";
                else
                    sflag = 1;

                media_list += "<option id='" + data.media_id + "' value='" + data.media_id + "'  title='" + data.name + "'" +selected_media + disable+">" + data.name + "</option>";
                noDataFound(data.testers.length);
            }
        });
    }

    media_dd.html("");
    media_dd.append(media_list).selectpicker('refresh');
    // user_dd.append(user_list).selectpicker('refresh').selectpicker('selectAll');

    var media_option_length = media_dd.find("option").length;
    if(media_option_length > 0) {
        //$('#media_dd select option:first-child').prop("selected", true);
        var mediaList = getSelectedMediaList();
        bind_video_image(media_dd,project_dd,get_project_list,mediaList)
    }

    $(".dashboard_image_wrapper").on("error", function(){
        var host_url =$("#host_url").val();

        $(this).attr('src', host_url+'static/img/dashboard/activity_type/25.jpg');
    });
    $('[data-toggle="tooltip"]').tooltip();

    function populateSummaryGraph() {
        $('[data-toggle="tooltip"]').tooltip({tooltipClass: "tooltiptext"});

        // $("#accordion_media_0,#accordion_media_1").accordion({
        //     collapsible: false,
        //     autoHeight: false
        // });


        // Return with commas in between
        var numberWithCommas = function (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        };

        var test_summary_url = api_host + "dashboard/avggraphs1/";


        users =  Array.from($('#user_dd').find("option:selected"));
        var user_fn = users => users.map(rec => rec['id'].split("_")[1]);
        user_selected = user_fn(users);
        ajax_data = {
            "media_id":getSelectedMediaList()
        }
        if(user_selected.length > 0)
            ajax_data.users =  user_selected;



        test_summary_data = call_ajax_json("POST", test_summary_url,JSON.stringify(ajax_data));
        chart_object_ref.graphDefaultData = test_summary_data;

        $("#accordion_media_0,#accordion_media_1").hide();
        var project_id = project_dd.children(":selected").attr("id").split("_")[1];
        var selected_project = get_project_list.filter(x => x.project_id == project_id);

        var media_in_project = selected_project[0].media;
        var media_list_filtered = media_in_project.filter(x => x.project_id == project_id);

        $('#chart_distribution_label_0,#chart_distribution_label_1').hide().text("");
        $("#chart_distribution_0,#chart_distribution_1").hide();
        for(var media_index = 0;media_index < test_summary_data.length; media_index++){
            if(test_summary_data.length == 1){
                $('#chart_distribution_container').css("padding-bottom", "0");
            }else{
                $(' #content').css("padding-top", "3px");
                // $("#cd_1").hide();
            }


            $("#accordion_media_"+media_index).show();
            var media_obj = media_list_filtered.filter(x => x.media_id == test_summary_data[media_index].media_id);
            var media_name = media_obj[0].name;
            var media_id = media_obj[0].media_id;
            $(".accordion_media_"+media_index).text(media_name).append("<a href='#' class='edit_btn right_align' target='_blank' onclick='download_test("+media_id+")'>Download</a>");
            $("#test_summary_wrapper_"+media_index).show().html("");
            $("#accordion_media_"+media_index).show();

            $('#chart_distribution_label_'+media_index).show().text(media_name);

            var media_id = test_summary_data[media_index].media_id;
            var attention = Math.round(test_summary_data[media_index].averages["attention"]);
            var complexity = Math.round(test_summary_data[media_index].averages["ME"]);
            var arousal = Math.round(test_summary_data[media_index].averages["ET"]);
            var impact = Math.round(test_summary_data[media_index].averages["AP"]);
            if(attention==0 && complexity==0 && arousal ==0 && impact == 0){
                attention = "-";
                complexity = "-";
                arousal = "-";
                impact = "-";
            }
            var replace_value={"Attention":"Attention","Arousal":"Activation","Complexity":"Engagement",
            "Impact":"Enjoyability","Activation":"Activation","Enjoyability":"Enjoyability","Engagement":"Engagement"};
            if(test_summary_data[media_index].averages["attention"] != undefined){

                var datajson = {
                    class:"attention_"+media_index,
                    id:"Attention",
                    value:attention,
                    title:"A cognitive variable that measures the users focus while watching a video, advertisement or listening to an audio. It provides an overview of how immersive the user is during this period",
                    sparkline_id:"attention_"+media_index
                }
                $("#test_summary_wrapper_"+media_index).append(build_widget(datajson,replace_value))
                var param = {
                    id: "attention_"+media_index+"_sparkline_line",
                    data: Object.values(test_summary_data[media_index].range_averages["attention"]),
                    tooltipChartTitle:"Attention"
                }
                // drawSparkLine(param);
            }
            if(test_summary_data[media_index].averages["ME"] != undefined){
                var datajson = {
                    class:"complexity_"+media_index,
                    id:"Complexity",
                    value:complexity,
                    title:"A key indicator of how a user's brain is processing the video, advertisement or audio. Higher the effort put in by the user to understand the content, greater the complexity and vice-versa",
                    sparkline_id:"complexity_"+media_index
                }
                $("#test_summary_wrapper_"+media_index).append(build_widget(datajson,replace_value))
                var param = {
                    id: "complexity_"+media_index+"_sparkline_line",
                    data: Object.values(test_summary_data[media_index].range_averages["ME"]),
                    tooltipChartTitle:"Complexity"
                }
                // drawSparkLine(param);
            }


            if(test_summary_data[media_index].averages["ET"] != undefined){
                var datajson = {
                    class:"arousal_"+media_index,
                    id:"Arousal",
                    value:arousal,
                    title:"A variable that ranges from tranquility or boredom to alertness and excitement. It corresponds to similar concepts such as level of activation and stimulation",
                    sparkline_id:"arousal_"+media_index
                }
                $("#test_summary_wrapper_"+media_index).append(build_widget(datajson,replace_value))
                var param = {
                    id: "arousal_"+media_index+"_sparkline_line",
                    data: Object.values(test_summary_data[media_index].range_averages["ET"]),
                    tooltipChartTitle:"Arousal"
                }
                // drawSparkLine(param);
            }
            if(test_summary_data[media_index].averages["AP"] != undefined){
                var datajson = {
                    class:"impact_"+media_index,
                    id:"Impact",
                    value:impact,
                    sparkline_id:"impact_"+media_index,
                    title:"A moment by moment analysis of how much of positive or negative inclination is displayed by user during the course of watching the video, advertisement or audio"
                }
                $("#test_summary_wrapper_"+media_index).append(build_widget(datajson,replace_value))
                var param = {
                    id: "impact_"+media_index+"_sparkline_line",
                    data: Object.values(test_summary_data[media_index].range_averages["AP"]),
                    tooltipChartTitle:"Impact",

                }
                // drawSparkLine(param);
            }

            $('[data-toggle="tooltip"]').tooltip();


        }
        var radarBackgroundColor = ["rgba(228, 164, 199,.5)","rgba(228, 164, 199,.5)"];
        var radarBorderColor= ["#e4a4c7","#26759b"];
        // ==== Radar Chart ===
        for(var record_index=0;record_index < test_summary_data.length;record_index++){
            var radar_chart_data = test_summary_data[record_index].radar_chart;
            var radar_chart_data_less_emo = test_summary_data[record_index].radar_chart_less_emo;

            if(radar_chart_data.hasOwnProperty("Happy")){
                $(".emotion_graph_"+record_index).show();


                // Less emotion radar chart
                lessEmotionRadarChart(record_index,radar_chart_data_less_emo);

                if(test_summary_data[record_index]["stacked_graph"].length != 0){

                    $("#chart_distribution_error_label_"+record_index).hide();
                    lessEmotionStackedBarGraph(record_index,test_summary_data[record_index]);

                    if("AP" in test_summary_data[record_index]["high_low"]){
                        stackBarTemplate(test_summary_data[record_index],record_index);
                    }
                    else{
                        // no data against the record
                        $("#chart_distribution_error_label_"+record_index).show();
                    }


                }
                else{
                    // Hide Emotion and Radar Graphs
                    $("#emotion_graph_error_label_"+record_index).show();
                    $(".emotion_graph_"+record_index).hide();
                }
            }
            else{
                // Hide Emotion and Radar Graphs
                $("#emotion_graph_error_label_"+record_index).show();
                $(".emotion_graph_"+record_index).hide();
            }

        }

    }

    function bind_video_image(media_dd,project_dd,get_project_list,media_list){
        $(".emotion_graph_0,.emotion_graph_1,#emotion_graph_error_label_0,#emotion_graph_error_label_1").hide();
        $("#selected_media_name_0,#selected_media_name_1").parent().parent().hide();
        var media_option_length =media_dd.find('option').length;
        $(".image_wrapper").hide();


        if(media_option_length > 0) {
            var project_id = project_dd.children(":selected").attr("id").split("_")[1];

            var selected_project = get_project_list.filter(x => x.project_id == project_id);
            $("#image_wrapper_heading_0,#image_wrapper_heading_1").remove();
            var media_in_project = selected_project[0].media;
            var media_list_filtered = media_in_project.filter(x => x.project_id == project_id);
            user_dd.html("").selectpicker('refresh');
            user_list = "";
            var unique_users_array  = new Set();
            for(var i=0;i<media_list.length;i++){
                // get media url
                var media_data = media_list_filtered.filter(x => x.media_id == media_list[i])[0];
                var media_url = media_data.url;
                $("#image_wrapper_"+i+",.emotion_graph_"+i).show();
                var media_name = media_data.name;
                // bind user dropdown again
                media_data.testers.forEach(function (rec) {
                    if(rec.name!=null) {
                        if (!unique_users_array.has(rec.id)) {
                            user_list += "<option id='usertester_" + rec.id + "' value='usertester_" + rec.id + "'  title='" + rec.name + "' >" + rec.name + "</option>";
                            unique_users_array.add(rec.id);
                        }
                    }
                });
                $("#selected_media_name_"+i).text("Emotional Spectrum :- "+media_name);
                $("#image_wrapper_heading_"+i).remove();
                $("#image_wrapper_"+i).html("").before("<label class='heading align' id='image_wrapper_heading_"+ i +"' style='background-color:"+rickColorConfig['Attention'][i]+"' title='"+media_name+"'>" + trimText(media_name)+"</label>");
                // if(media_data.activitytype_id != 3){
                    if(media_url != "" && media_url != null && media_data.activitytype_id == 1){
                        $("#image_wrapper_"+i).append("<video id='homeScreenVideo' src='"+
                            media_url+ "' class='dashboard_image_wrapper' controls>")
                        //"onplay='onSeekingHomeScreenVideo()' onpause='stopCursor()' controls>")
                    }else{
                        $("#image_wrapper_"+i).append("<img class='dashboard_image_wrapper'" +
                            " src='"+host_url + "'static/img/dashboard/activity_type/25.jpg' />");
                        $(".dashboard_image_wrapper").on("error", function () {
                            $(this).attr('src',host_url+ 'static/img/dashboard/activity_type/25.jpg');
                        });
                    }
                //}

            }
            noDataFound(media_list.length);
            noDataFound(unique_users_array.size);


            user_dd.append(user_list).selectpicker('refresh').selectpicker('selectAll');
            populateSummaryGraph();
            populate_graph();

        }

    }

    function drawSparkLine(param) {
        $("#" + param.id).sparkline(array_value_round(param.data), {
            type: 'line',
            width: '100%',
            height: '40',
            spotRadius: 0,
            lineWidth: 1,
            lineColor: '#ffffff',
            fillColor: false,
            minSpotColor: false,
            maxSpotColor: false,
            highlightLineColor: '#ffffff',
            highlightSpotColor: '#ffffff',
            tooltipChartTitle: param.tooltipChartTitle,
            tooltipPrefix: '',
            spotColor: '#ffffff',
            valueSpots: {
                '0:': '#ffffff'
            }
        });
    }
}); //end of document ready


    $("#full_screen_mode").on('shown.bs.modal', function (e) {

        var video = $('.image_wrapper').parent().html();
        var chart = $('#chart_container').html();


        $('#full_screen_mode_video').html(video);
        $('#full_screen_mode_graph').html(chart);
    });

function populate_screen_video(index,media_id,user_id){
    var url = 'https://s3-ap-southeast-1.amazonaws.com/affectlab/screen_recording/'+media_id+"/"+user_id+"/"+'1.mp4';
        $("#image_wrapper_"+index).empty().append("<video id='homeScreenVideo' src='"+
            url+"' class='dashboard_image_wrapper' controls>");

}

// widget template
function build_widget(json,replace_value){
    return '<div class="col-xs-3">'+
        '<div class="dash_card card"><div class="content dash_content">' +
        '<h4 class="summary_title">'+ replace_value[json.id]+'</h4>'+
        '<div class="row"><div class="col-xs-3"></div>' +
        '<div class="col-xs-6">'+
        '<div class="c100 p'+json.value+' myfav small"> <span class="text-bold">'+json.value+'</span>'+
        '<div class="slice"><div class="bar"></div><div class="fill"></div></div></div>'+
        '</div><div class="col-xs-3"></div></div></div></div>';
}

function array_value_round(input_array){
    var array = []
    for(i=0;i<input_array.length;i++){
        array.push(Math.round(input_array[i]));
    }
    return array;
}

function getSelectedMediaList() {
    var medias  = [];
    // if the media dropdown have any media, it will come here.
    medias = Array.from($('#media_dd').find("option:selected"));
    var media_fn = medias=> medias.map(rec => parseInt(rec['id']));
    medias = media_fn(medias);
    return medias;
}

function stackBarTemplate(data,media_index){
    $('#chart_distribution_'+media_index,"#chart_wrapper").show();
    var topElementHeight = $('#graph_zone > div.page-content').height();
    var height = calculateLeftPanelheight() -topElementHeight- 88;
    $('#content').css('height',height+'px');

    // selected Metrics value
    var selectedMetric ="attention";
    $(".research_parameters .selected_param").each(function(i, v){
        selectedMetric = $(this).attr("val");
    });
    var stackBarLabel = [];
    var hasDistributionProperty = "distribution" in data["high_low"][selectedMetric];
    if(hasDistributionProperty) {
        var stackBarData = data["high_low"][selectedMetric]["distribution"];
        if (stackBarData.length != 0) {
            var dataSetList = [
                {
                    label: 'Low',
                    data: [],
                    backgroundColor: "#da1d83",
                    hoverBackgroundColor: "#da1d83",
                    hoverBorderWidth: 2,
                    hoverBorderColor: 'lightgrey'
                },
                {
                    label: 'Medium',
                    data: [],
                    backgroundColor: "#26759b",
                    hoverBackgroundColor: "#26759b",
                    hoverBorderWidth: 2,
                    hoverBorderColor: 'lightgrey'
                },
                {
                    label: 'High',
                    data: [],
                    backgroundColor: "#6ec53c",
                    hoverBackgroundColor: "#6ec53c",
                    hoverBorderWidth: 2,
                    hoverBorderColor: 'lightgrey'
                },
                {
                    label: 'Very High',
                    data: [],
                    backgroundColor: "#c2bb1b",
                    hoverBackgroundColor: "#c2bb1b",
                    hoverBorderWidth: 2,
                    hoverBorderColor: 'lightgrey'
                }
            ];
            //set dataset value and label
            for (var stack_bar_index = 0; stack_bar_index < stackBarData.length; stack_bar_index++) {
                var stack_data = stackBarData[stack_bar_index];
                dataSetList[0].data.push(stack_data["low"].toFixed(2));
                dataSetList[1].data.push(stack_data["medium"].toFixed(2));
                dataSetList[2].data.push(stack_data["high"].toFixed(2));
                dataSetList[3].data.push(stack_data["very high"].toFixed(2));
                stackBarLabel.push(formatTime(stack_data["second"]));
            }

            if (chart_object_ref.distribution.length == media_index) {
                var chartDistributionCtx = document.getElementById('chart_distribution_' + media_index).getContext('2d');
                chart_object_ref.distribution.push({"chart": buildStackChart(chartDistributionCtx,dataSetList,stackBarLabel)});
            }
            else {

                addData(chart_object_ref.distribution[media_index].chart, dataSetList,stackBarLabel)
            }

            return false;
        }
    }
    $('#chart_distribution_'+media_index).hide();
    $("#chart_distribution_error_label_"+media_index).show();
}

function buildStackChart(ctx,dataSet,label){
    var chartInstance = new Chart(ctx, {
        type: 'bar',
        scaleOverride: true,
        scaleSteps: 20,
        scaleStartValue: 0,
        scaleStepWidth: 100,
        data: {
            labels: label,
            datasets: dataSet
        },
        options: {
            responsive:true,
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function (tooltipItem, local_data) {
                        return local_data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Time in (HH:MM:SS)'
                    },
                    stacked: true,
                    gridLines: {display: false},
                    ticks: {
                        stepSize: 20,

                    }
                }],
                yAxes: [{
                    maxTick: 100,
                    ticks: {
                        stepSize: 20,

                    },
                    stacked: true,
                    ticks: {
                        callback: function (value) {
                            return numberWithCommas(value);
                        },
                        max: 100,
                        min: 0
                    },
                }],
            }, // scales
            legend: {display: true}
        } // options
    });
    return chartInstance;
}
function numberWithCommas(x){
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function addData(chart, data,stackLabel) {
    chart.data.datasets.forEach(function(dataset) {
        chart.data.datasets.pop();
    });
    if(stackLabel != null){
        chart.data.labels = [];
        chart.data.labels = stackLabel;
    }
    chart.data.datasets = data;
    chart.update();
}
function calculateLeftPanelheight(){
    return $('.left_pane').height();
}
function noDataFound(length){
    $cacheObj = $('#graph_zone,.chart-footer');
    if(length ==0 )  {
        $cacheObj.hide()
        $('.no-data-found-message').show();
    }else{$cacheObj.show();
        $('.no-data-found-message').hide();
    }
}
function trimText(value){
    if(value.length >40){
        return value.slice(0,39)+"..."
    }
    return value;
}

function lessEmotionStackedBarGraph(row,emotionData) {
    var emotionStackBarLabel = [];
    var emotionBarDataSet = [
        {
            label: 'Happy',
            data: [],
            backgroundColor: "#da1d83",
            hoverBackgroundColor: "#da1d83",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        {
            label: 'Relaxed',
            data: [],
            backgroundColor: "#26759b",
            hoverBackgroundColor: "#26759b",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        {
            label: 'Neutral',
            data: [],
            backgroundColor: "#ca46cd",
            hoverBackgroundColor: "#ca46cd",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        {
            label: 'Sad',
            data: [],
            backgroundColor: "#6ec53c",
            hoverBackgroundColor: "#6ec53c",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        {
            label: 'Tense',
            data: [],
            backgroundColor: "#00bcd4",
            hoverBackgroundColor: "#00bcd4",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        }
    ];


    for(var stack_bar_index = 0;stack_bar_index<emotionData["stacked_graph_less_emo"].length;stack_bar_index++){
        var data = emotionData.stacked_graph_less_emo[stack_bar_index];
        for(var emotionDataSetIndex = 0;emotionDataSetIndex<emotionBarDataSet.length;emotionDataSetIndex++) {
            var dataSet = data[emotionBarDataSet[emotionDataSetIndex]["label"]].toFixed(2);
            emotionBarDataSet[emotionDataSetIndex].data.push(dataSet);
        }
        emotionStackBarLabel.push(formatTime(data["second"]));
    }

    if(chart_object_ref.less_emotion.length == row ) {
        var emotionBarCtx = document.getElementById('emotion_bar_chart_less_emo_'+row).getContext('2d');
        chart_object_ref.less_emotion.push({
            "chart_less":buildStackChart(emotionBarCtx,emotionBarDataSet,emotionStackBarLabel)
        });
    }
    else{
        addData(chart_object_ref.less_emotion[row].chart_less,emotionBarDataSet,emotionStackBarLabel);
    }


}

function lessEmotionRadarChart(row,radar_chart_data){
    var radarBackgroundColor = ["rgba(228, 164, 199,.5)","rgba(228, 164, 199,.5)"];
    var radarBorderColor= ["#e4a4c7","#26759b"];
    var radarChartDataSet = [{
        data: [
            radar_chart_data["Neutral"].toFixed(2),
            radar_chart_data["Sad"].toFixed(2),
            radar_chart_data["Tense"].toFixed(2),
            radar_chart_data["Happy"].toFixed(2),
            radar_chart_data["Relaxed"].toFixed(2),
            ],
        fill:true,
        backgroundColor:radarBackgroundColor[row],
        borderColor:radarBorderColor[row],
        pointBackgroundColor:radarBorderColor[row],
        pointBorderColor:"#fff",
        pointHoverBackgroundColor:"#fff",
        pointHoverBorderColor:radarBorderColor[row]}];
    if(chart_object_ref.radar_less_emotion.length == row ){
        buildLessEmotionRadarChart(row,radarChartDataSet)
    }else{
        addData(chart_object_ref.radar_less_emotion[row].chart,radarChartDataSet,null)
    }
}

function buildLessEmotionRadarChart(record_index,radarChartDataSet){
    var radarChartCtx = document.getElementById('radar-chart_less_emo_'+record_index).getContext('2d');
    var radarChartInstance = new Chart(radarChartCtx, {
        type: 'radar',
        height:200,
        data: {
            labels: ['Neutral','Sad','Tense','Happy','Relaxed'],
            datasets: radarChartDataSet
        },
        options: {
            legend: {
                display: false
            },
        }
    });
    chart_object_ref.radar_less_emotion.push({"chart":radarChartInstance});
}
