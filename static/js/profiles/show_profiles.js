
function call_ajax1(url,ajax_data)
{
          var data = [];
           $.ajax({
            url : url,
            type : 'POST',
            data: ajax_data,
            async: false ,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRFToken',Cookies.get('csrftoken'))
            },
            success : function(response) {
                data = response;
            }
        });
    return data;
}

function regenerate_api_key()
{
    var  API_URL = $('#api_url').val();
    var user_id = $('#user_id').val();
    var url = API_URL + "dashboard/get_api_key/"
    ajax_data = {"user_id":user_id};
     data = [];
     data = call_ajax1(url,ajax_data);
        for(let key of data)
        {
            call_ajax1(API_URL+"dashboard/delete_api_key/",{"user_id":user_id,"api_key":key});
        }
    url = API_URL +"dashboard/generate_api_key/";
    api_key = call_ajax1(url,{"user_id":user_id,"key_name":"default"});
    $("#my_keys").empty();
    $("#my_keys").append("<strong>"+api_key['api_key']+"</strong>")

};
$(function () {
        var  API_HOSTNAME = $('#api_host').val();
        var  API_URL = $('#api_url').val();
        var user_id = $('#user_id').val();
        var data= [];

    try {
        ajax_data = {"user_id":user_id};
        url = API_URL + "dashboard/get_api_key/"
        data = call_ajax1(url,ajax_data);
        var flag = 0;
        for(let key of data)
        {
            if(flag==0)
            {
                $("#api_key_hidden").show();
                flag=1;
            }
            $("#my_keys").append("<strong>"+key+"</strong>");
        }

    }
    catch (error) {
            console.log(error);
        }
    });
