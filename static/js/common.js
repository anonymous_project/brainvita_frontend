/**
 * Application Common Global Variables
 */
var GLOBAL_FILTER_COUNT = 1;
var GENRIC_FILTER_UNIQUE_ID = "gfuid";
// var PROJECT_TOKEN = "5a26b01e-622d-4512-aba4-158651cd0846";
var FILTER_JSON_ARRAY = [];

/** Replace window url with mask url
 * @parameters : url,project_token, masking_id
 */
function url_mask(url,project_token, masking_id){
    var url_mask = url.replace(/project_token/, project_token.toString()).replace(/dummy_id/, masking_id.toString());
    window.location.replace(url_mask);
}

/**  Generate 1-D array with random data
 *   @return : array
 */
function random_data_generator(){
    var random_data = [];
    for(var i=0; i<10; i++){
        random_data.push(Math.floor((Math.random() * 100) + 1));
    }
    return random_data;
}

/**  Get drop down selected value based on id
 *   @param : id
 *   @return : string
 */
function get_drop_down_value_by_id(id){
    return $('#'+id).val();
}

/**  Get textbox value based on id
 *   @param : id
 *   @return : string
  */
function get_textbox_value_by_id(id){
    return $('#'+id).val();
}

/** Create filter json filter template
 * @parameters : name, type, filter_by, filter_value, operator
 * @return : json object
 */
function filter_json_template(name, type, filter_by, filter_value, operator){
   if(operator != "" && operator != undefined) {
       return {
            name       : name,
            type       : type,
            filterby   : filter_by,
            filtervalue: filter_value,
            operator   : operator
        };
    }
    else{
        return {
            name       : name,
            type       : type,
            filterby   : filter_by,
            filtervalue: filter_value
        };
    }

}
/**
 * Set AND OR based on value checkbox value
 * @param is_operator_checked
 * @returns {string: and/or/""}
 */
function set_operator(is_operator_checked){
    if(is_operator_checked == true){
        return "and"
    }
    else if(is_operator_checked == false){
        return "or";
    }else{
        return "";
    }
}
/**
 * Hightlight selected tag based on url.
 */
$(function () {
  var selectedUrl = window.location.pathname;
    $("a[href$='"+ selectedUrl +"']").parent().addClass('active')
});