	var api_host = $("#api_url").val();
	// var current_domain_url = "";
	var current_domain_url = $("#host_url").val();
	var user_id = $("#user_id").val();
	var project_id=undefined;
$(function(){
	$(".navbar-brand").text("Projects");
	$("#goback").remove();
	var projects_container = $(".project_list_container");
	var tests_container = $(".media_list_container");
	//get project list api
    var project_url = api_host + "dashboard/projectoverview/";
    var ajax_data = {
    	"user_id": user_id,
        "select": ["attention", 'AP', 'YY', 'ET', 'ME']
    };
    /*$('.project_list_container').ajaxStart(function(){
    	$('#loader').show();
    });*/
	var get_project_list = call_ajax("POST", project_url, ajax_data);
    /*$('.project_list_container').ajaxComplete(function(){
    	$('#loader').hide();
    });*/
	if (get_project_list.length > 0) {
		$.each(get_project_list, function(key,value){
			projects_container.append("<div  class='col-lg-3 col-sm-6'><div class='card project_strip'>"+
												"<div id='"+value.project_id+"' data-pname='"+value.project_name+"'  class='content project_card'><div class='row'>"+
												"<div class='col-xs-12' ><div class='icon-small icon-primary project_icon'>"+"<i class='ti-file'></i>"+"</div></div>"+
												"<div class='col-xs-12'><h5 class='pname'>"+
												value.project_name+
												"</h5>"+"<div style='margin:0 0 7px 10px;'>"+
												"<a href='#' class='edit_btn' title='Edit Project' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-row='" + key+ "' data-projectid='" + value.project_name + "' data-category='"+value.category+"' data-project='"+value.project_id+"' data-subcategory='"+value.subcategory+"' data-target='#addProjectModal' data-modaltype='Edit Project'>Edit</a>"+"&nbsp;&nbsp;"+
												"<a href='#' class='edit_btn' title='Delete Project' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-row='" + key+ "' data-projectid='" + value.project_name + "' data-category='"+value.category+"' data-project='"+value.project_id+"' data-subcategory='"+value.subcategory+"'    data-target='#deleteProjectModal'>Delete</a>"+"&nbsp;&nbsp;"+
												"<span class='delete_btn view_projects' data-project='"+value.project_id+"' title='View Project' >View</span>"+
												"</div></div></div></div></div></div>");
		});
	}
	//On clicking the PRoject card this has to be performed
	$(".project_card, .view_projects").on('click',function(event){
		event.preventDefault();
		//Do nothing if edit or delete link is clicked
	    if(event.target.nodeName === "A") return;
		event.stopPropagation();
		(event.target.nodeName === "SPAN") ? project_id = $(this).attr('data-project') : project_id = $(this).attr("id");
		$(".navbar-brand").text($(this).data('pname'));
		projects_container.hide();
		$("#goback").remove();
		tests_container.show();
		tests_container.before("<div id='goback'><<&nbsp;Back to Projects</div>");
		var media_data_url = api_host + "dashboard/mediaoverview/";
		var test_ajax_data = {"project_id": project_id};
		$("#pid").val(project_id);
		var get_test_list = call_ajax("POST", media_data_url, test_ajax_data);
		if(get_test_list.length > 0){
			var atype ="#f1c40f";
		$.each(get_test_list, function(key,value){
			switch(value.activity_type){
				case 1: atype = "#3498db";break;
				case 15: atype = "#1abc9c";break;
				case 20: atype = "#f1c40f";break;
				case 30: atype = "#f1c40f";break;
				case 35: atype = "#f1c40f";break;
				case 40: atype = "#f1c40f";break;
			}
			tests_container.append("<div id='"+value.media_id+"' style='cursor:pointer;'"+
					"class='col-lg-6 col-sm-6 test_card'><div class='card' style='border-top:7px solid "+atype+";'>"+
					"<div class='content'><div class='row'>"+
					"<div class='col-xs-12'>"+
					"<div class='col-xs-3'>"+
					"<div class='c100 p"+Math.round(value.content_analysis['attention'])+
					" green small'><span title='Attention'>"+value.content_analysis['attention']+
					"%<p class='tooltip_text'>Attention</p></span><div class='slice'><div class='bar'></div>"+
					"<div class='fill'></div></div></div></div>"+
					
					"<div class='col-xs-3'>"+
					"<div class='c100 p"+Math.round(value.content_analysis['ME'])+
					" orange small'><span title='Engagement'>"+
					value.content_analysis['ME']+"%<p class='tooltip_text'>Engagement</p></span><div class='slice'><div class='bar'></div>"+
					"<div class='fill'></div></div></div></div>"+

					"<div class='col-xs-3'>"+
					"<div class='c100 p"+Math.round(value.content_analysis['ET'])+" pale small'><span title='Activation'>"
					+value.content_analysis['ET']+"%<p class='tooltip_text'>Activation</p></span><div class='slice'><div class='bar'></div>"+
					"<div class='fill'></div></div></div></div>"+

					"<div class='col-xs-3'>"+
					"<div class='c100 p"+Math.round(value.content_analysis['AP'])+
					" yellow small'><span title='Enjoyability'>"+value.content_analysis['AP']+
					"%<p class='tooltip_text'>Enjoyability</p></span><div class='slice'><div class='bar'></div>"+
					"<div class='fill'></div></div></div></div>"+
					
					"</div></div>"+
					"<div><br /><p class='pname tname text-center'>"+
					value.media_name+
					"</p></div>"+
					"<div class='progress'>"+
					"<div class='progress-bar' style='width:"+value.status+"%'>"+
 					// "<span class='sr-only'>70% Complete</span>"+
					"</div></div><div class='edit_row test_footer'><div class=''>"+
					"<a href='#' class='edit_btn' title='Track Test' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-activity='"+value.activity_type+"'  data-project='"+value.project_id+"' data-media='"+value.media_id+"' data-target='#trackTestModal' data-modaltype='Track Test'>Track</a>"+"&nbsp;&nbsp;"+
					"<a href='#' class='edit_btn' title='Edit Test' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-activity='"+value.activity_type+"'  data-project='"+value.project_id+"' data-media='"+value.media_id+"' data-target='#editTestModal' data-modaltype='Edit Test'>Edit</a>"+"&nbsp;&nbsp;"+
					"<a href='#' class='edit_btn' title='Delete Test' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-project='"+value.project_id+"' data-media='"+value.media_id+"'    data-target='#deleteTestModal'>Delete</a>"+"&nbsp;&nbsp;"+
					"<a href='#' class='edit_btn' title='Download Test' onclick='download_test("+value.media_id+")' >Download</a>"+"&nbsp;&nbsp;"+
					"<a href='/users/dashboard/"+value.project_id+"/"+value.media_id+"' class='delete_btn' >Results</a>"+
					"</div></div></div></div></div></div>");
			});
		}
	    var url = window.location.href;
	    var pop_id = url.split("/").pop();
	    if(event.target.nodeName === "SPAN" && $(event.target).hasClass("view_projects") && pop_id === "") 
	    {
			window.history.replaceState(null, null, url+project_id);
			return;
	    }	
	    else if(pop_id === ""){
			window.history.replaceState(null, null, url+this.id);
	    }
	});
		
		$(document).on('click','#goback',function(){
			$(this).hide();
			$(".navbar-brand").text("Projects");
			$('.media_list_container').hide();
			var clone=$('.media_list_container .first_create').clone();
			$('.media_list_container').html("").append(clone);
			$('.project_list_container').show();

			var end_url =window.location.href.split("/project_overview/").pop();
			var url = window.location.href;
			var a = url.indexOf("/"+end_url);
    		var b =  url.substring(a);
    		var c = url.replace(b,"/");
			window.history.replaceState(null, null,c);
			// tests_container.hide();
			// projects_container.show();
		});
		var pvid = $("#url_project_id").val();
		if(pvid != ''){
			$("#"+pvid).trigger("click");
		}

		$("#addTestModal").on('show.bs.modal', function (event) {
		    $(this).appendTo('body');
		}).on('shown.bs.modal', function (event) {
			// project_id = getDataAttribute(event, 'project');
			project_id = $("#pid").val();
		    $('#add_test_modal_content').load(current_domain_url+'users/add_test?project_id='+project_id);
		});

		$("#editTestModal").on('show.bs.modal', function (event) {
		    $(this).appendTo('body');
		}).on('shown.bs.modal', function (e) {
		    project_id = getDataAttribute(e, 'project');
		    //name = getDataAttribute(e, 'name');
		    //console.log(project_id);
		    $('.modal-footer .save_btn').val("Update");
		    media_id = getDataAttribute(e, 'media');
		    var activity_type = getDataAttribute(e, 'activity');
		     $('#edit_test_modal_content,#add_test_modal_content').html('');
		    $('#edit_test_modal_content').load(current_domain_url+'users/edit_test?media_id='+media_id+'&project_id='+project_id+'&activity_type='+activity_type);
		});

		// for creating or editing new project
		$("#addProjectModal").on('show.bs.modal', function (event) {
		    var modalType = getDataAttribute(event, 'modaltype');
		    $('#addProjectModal').find('.modal-title').text(modalType);
		    $(this).appendTo('body');
		}).on('shown.bs.modal', function (event) {
		        var modalType = getDataAttribute(event, 'modaltype');
		        if (modalType == 'Edit Project') {
			            var project_id = getDataAttribute(event, 'project');
		        	$('#add_project_modal_content').load(current_domain_url+'users/add_project/'+project_id, function () {
			            var row = getDataAttribute(event, 'row');
			            var project_name = getDataAttribute(event, 'projectid');
			            var category = getDataAttribute(event, 'category');
			            var subcategory = getDataAttribute(event, 'subcategory');
			            var data ={project_name,category,subcategory};
			            populateProjectData(data);
			            $('#addProjectModal').find('input[type="button"]').val("Update Project");
		        	});
		        }else{
		        	$('#add_project_modal_content').load(current_domain_url+'users/add_project/', function () {
		            		$('#addProjectModal').find('input[type="button"]').val("Create Project");
		        	//add project 
		            // $('#addProjectModalContent').find('#project_id').val("").find('#row_index_id').val("");
		    		});
		        }

		});


		$("#trackTestModal").on('show.bs.modal', function (event) {
		    $(this).appendTo('body');
		}).on('shown.bs.modal', function (e) {
			var project_id = $(e.relatedTarget).data('project');
		    var media_id = $(e.relatedTarget).data('media');
		    $('#track_test_modal_content').load(current_domain_url+'users/track_test/' + project_id + '/' + media_id);
		});

		$("#deleteTestModal").on('show.bs.modal', function (event) {
		    $(this).appendTo('body');
		}).on('shown.bs.modal', function (e) {
		    media_id = $(e.relatedTarget).data('media');
		});
		// for deleting the project modal
		$("#deleteProjectModal").on('show.bs.modal', function (event) {
		    $(this).appendTo('body');
		}).on('shown.bs.modal', function (event) {
			project_id = getDataAttribute(event, 'project');
            // var row_id = getDataAttribute(event, 'row');
		    // var $dataTableCache = $('#project_table').DataTable();
		    // var row_index_id = $dataTableCache.data().length - row_id -1
		    // var data = $dataTableCache.rows().data()[row_id];
		    // project_id = data.project_id;
		});
		    
	 });//end of document ready  
function getDataAttribute(event, attrName) {
    var button = $(event.relatedTarget); // Button that triggered the modal
	return button.data(attrName); // Extract info from data-* attributes
}

function download_test(media_id)
{
	var media_data_url = api_host + "dashboard/download_test/";
    var download_test_data = call_ajax_text("POST", media_data_url, {media_id: media_id});
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(download_test_data);
    // hiddenElement.target = '_blank';
    hiddenElement.download = 'data_test_'+media_id+'.csv';
    hiddenElement.click();
    // var uri = 'data:application/csv;charset=UTF-8,'+encodeURIComponent(download_test_data);
    // window.open(uri,'data.csv');
}
function deleteTest() {
    var media_data_url = api_host + "dashboard/delete_media/";
    var detailsRowData = call_ajax("POST", media_data_url, {media_id: media_id});
    if (detailsRowData != undefined && detailsRowData.status == "deleted") {
        // $(this).parents('tr').remove();
        $("#"+media_id).remove();
    }
    //$('#deleteTestModal').dialog().dialog('close');
    // window.location.reload();
    return false;
}
function deleteProject() {
    var project_data_url = api_host + "dashboard/createentity/delete/project/";
    var detailsRowData = call_ajax("POST", project_data_url,{"id":project_id});
    if (detailsRowData != undefined && detailsRowData.status == "deleted") {
        // $(this).parents('tr').remove
        $("#"+project_id).parent().parent().remove();
    }
    // window.location.reload();
    return false;
}
function addNewProject(row) {
    var data =
    {
        "project_id": row.projectId,
        "project_name": row.projectName,
        "category": row.category,
        "subcategory": row.subcategory
    };
    $("#project_table").DataTable().row.add(data).draw();
}
function upateProject(row_id,data) {
   // $("#project_table").DataTable().row(row_id).data(data).draw();
}      
