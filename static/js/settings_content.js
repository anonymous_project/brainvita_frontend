function toggle_deactivate(user_id)
{
        var api_url = $("#api_url").val();
        status=ajax_call_with_login("POST",api_url+"dashboard/toggle_deactivate_user/",{"user_id":user_id});
        console.log(status);
}

function escapeRegExp(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}
var project_id="";
var project_list="";
var media_id = "";
// Document ready jquery functions
$(function () {
    var sub_cat_to_be_displayed = "";
    $("#tabs").tabs().css({
        'min-height': '500px',
        'overflow': 'auto',
        'display':'block'
    });
    if($(location).attr('pathname')== "/users/settings/"){
       var selected =$(".sidebar-wrapper ul.nav");
        selected.find("li.active").removeClass("active");
        selected.find("li:nth-child(3)").addClass("active");
    }
    //get the api request url
    var api_url = $("#api_url").val();
    //get logged in admin details
    var admin_id = $("#user_id").val();
    //change this value to limit the number of new input fields
    var max_fields= 30;
    //maximum input boxes allowed

    /**
    * --------------------------------------
    *  Tester Module
    * --------------------------------------
    */
    //for adding multiple emails
    var email_fields     = $(".email_fields");
    var email_wrapper = $(".input_emails_wrap"); //Fields wrapper
    var add_emails_button= $(".add_email_button"); //Add options button
    var emails_count = email_fields.length; //initial text box count
    $(add_emails_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(emails_count < max_fields){
            emails_count++; //text box increment
            //add input box
            $(email_wrapper).append('<div><input type="email" required name="email" ' +
                'placeholder="Enter Valid Email Address" class="form-control email_fields">' +
                '<a href="#" class="remove_field"><img class="form-control" src="/static/img/remove-icon.png"></a>' +
                '</div>');
        }
    });
    $(email_wrapper).on("click",".remove_field", function(event){ //user click on remove text
        event.preventDefault();
        $(this).parent('div').remove();
        emails_count--;
    });
    //end of emails

    var form_data = $('form[name="create_tester_form"]');
    form_data.on("submit",function (event) {
        event.preventDefault();
        var add_tester_url = api_url+"dashboard/createtester/";
        var email = $("input[name='email']")
            .map(function(){return $(this).val();}).get();
        var ajax_data = {
            "emails":email,
            "admin_id":admin_id
        };
        $(this).find(".success_message").text("Creating...");
        var sender_status = ajax_call_with_login("POST",add_tester_url,ajax_data);
        if(sender_status.status == "Created")
        {
            //window.location= "/users/home";
            $(this).find("input[type=email]").not(":first").hide().siblings().hide().val("");
            // $(this).find("input[type=email]").val("");
            $(this).find(".success_message").html("Tester Created Successfully. " +
                "<br>Sent email for all the tester account(s)");
        }
        else
             $(this).find(".success_message").html(
                "<br>"+sender_status.status);
        return false;
    });
    //end of form submit.
    //Display list of tester
    var tester_table =$("#tester_table");
    var get_testers_list_url = api_url+"dashboard/getusertesters/";
    var get_tester_ajax_data = {"user_id":admin_id};
    var get_user_testers = call_ajax("POST",get_testers_list_url, get_tester_ajax_data);
    var checked = "checked";
    if(get_user_testers.length > 0){
        var header_row = "<tr>";
            header_row += "<td>Name</td><td>Email</td><td>Age</td><td>Gender</td>" +
                "<td>Activate/Deactivate</td>";
        get_user_testers.forEach(function (tester) {
            if(tester.is_active==false)
                checked = "";
            else
                checked = "checked";
            var body_row="<tr>";
            delete_tester = "<div align=center><input style='display: none;' class='btn-default off' "+checked+" data-toggle='toggle' data-on='Activate' data-off='Deactivate'"+
             "data-style='ios' data-size='mini' data-height='30' data-width='100' type='checkbox' id='toggle_deactivate_'"
             +tester.user_id+" onchange='toggle_deactivate("+tester.user_id+");'></div>";
            body_row += "<td class='tester_name' val='"+tester.user_id+"'>"+tester.name+"</td><td>"+tester.email+"</td>" +
                "<td>"+tester.age+"</td><td>"+tester.gender+"</td>" +
                "<td>" +delete_tester+"</td>";
            body_row +="</tr>";
            tester_table.find("tbody").append(body_row);
        });
        header_row +="</tr>";
        tester_table.find("thead").append(header_row);
    }
    else{
        tester_table.siblings("h3").text("");
        tester_table.find("thead").append("<h4>No Tester Exist</h4>");
    }
    //end of list display
    $(".cancel_btn").on('click',function (event) {
        event.preventDefault();
        $(this).parent().find(".success_message").html("");
        $(this).parent().find("input[type=text], textarea").val("");
        $(this).parent().find("input[type=email],textarea").val("");
        $(this).parent().find("input[type=email]").not(":first").hide().siblings().hide().val("");
       // $(this).parent().parent().addClass("hide");
    });
    //end of Tester Module.

    /**
    * ---------------------------------------  
    * Category Module
    * ---------------------------------------  
    */
    /*For Sub categories*/
    var sub_categories_wrapper = $(".input_sub_categories_wrap"); //Fields wrapper
    var add_sub_categories_button= $(".add_field_button"); //Add options button
    var sub_categories    = $(".subcategories"); //
    var sub_categories_count = sub_categories.length; //initial text box count
    $(add_sub_categories_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(sub_categories_count < max_fields){ //max input box allowed
            sub_categories_count++; //text box increment
            //add input box
            $(sub_categories_wrapper).append('<div><input type="text" val="0" required name="sub_categories"  ' +
                'placeholder="Enter Subcategory Name" class="form-control subcategories"/>' +
                '<a href="#" class="remove_field"><img class="form-control" src="/static/img/remove-icon.png"></a>' +
                '</div>');
        }
    });
    $(sub_categories_wrapper).on("click",".remove_field", function(event){ //user click on remove text
        event.preventDefault();
        $(this).parent('div').remove();
        sub_categories_count--;
    });
    /*end of sub categories */
    //form submit for categories and sub categories
    var ajax_data = {};
    var subcategories_list=[], add_subcategories = [];
    var categories_form_data = $('form[name="create_categories"]');
    categories_form_data.on("submit",function (event) {
        event.preventDefault();
        var sub_categories_selector = $("input[name='sub_categories']");
        var add_categories_url = api_url+"dashboard/querycategories/";
        var status_text = $(".categories_btn").val();
        if(status_text == "Update")
        {
            var category_name = $("#category_name");
            $(this).find(".success_message").text("Updating...");
            sub_categories_selector.each(function (data) {
                if(parseInt($(this).attr("val")) == 0)
                {
                    add_subcategories.push($(this).val())
                }
                else {
                    subcategories_list.push({"subcategory": $(this).val(), "subcategory_id": parseInt($(this).attr("val"))
                        });
                }
            });
            var display_category_name = category_name.val();
            if(category_name.val().length>30)
            {
              display_category_name = category_name.val().substring(0,30)+"...";
            }
            $("#c_"+category_name.attr("val")).text(display_category_name);
            sub_cat_to_be_displayed = Array.from(new Set(subcategories_list)).map(key => key['subcategory']).join();
            sub_cat_to_be_displayed = replaceAll(sub_cat_to_be_displayed,",",", ");
            ajax_data = {
                "op":"update",
                "category":category_name.val(),
                "category_id":parseInt(category_name.attr("val")),
                "subcategories":Array.from(new Set(subcategories_list)),
                "add_subcategories":Array.from(new Set(add_subcategories)),
                "user_id":parseInt(admin_id)
            };

            subcategories_list = [];add_subcategories=[];
        }
        else {
            $(this).find(".success_message").text("Creating...");
           subcategories_list =sub_categories_selector.map(function(){return $(this).val();}).get();
            ajax_data = {
                "op": "add",
                "category": $("#category_name").val(),
                "subcategories": subcategories_list,
                "user_id": admin_id
            };
        }
        var sender_status = ajax_call_with_login("POST",add_categories_url,ajax_data);
        ajax_data = {};
        if(sender_status.status == "added" )
        {
            $(this).find(".success_message").text(status_text+"d Successfully").fadeIn(3000);
            $(this).find(".success_message").text(status_text+"d Successfully").fadeOut(3000);
            $(this).find("input[type=text], textarea").val("");
            if(subcategories_list.length > 1){
                $(".input_sub_categories_wrap div").not(':first').remove();
            }
            // window.location= "/users/home";
        }
        else if(sender_status.status == "updated"){
            cat_id = category_name.attr("val");
            if(sender_status.status=="updated")
            {
                sub_cat_to_be_displayed=sub_cat_to_be_displayed;
                $("#c_"+cat_id).next().attr('title',sub_cat_to_be_displayed);

                if(sub_cat_to_be_displayed.length>30)
                   sub_cat_to_be_displayed = sub_cat_to_be_displayed.substring(0,30)+"...";
                $("#c_"+cat_id).attr('title',category_name.attr("value"));
                $("#c_"+cat_id).next().text(sub_cat_to_be_displayed);
            }
            $(this).find("input[type=text], textarea").val("");
            $(this).find(".success_message").text(sender_status.status).fadeIn(3000);
            $(this).find(".success_message").text(sender_status.status).fadeOut(3000);
            if(subcategories_list.length > 1){
                $(".input_sub_categories_wrap div").not(':first').remove();
            }
            // $(".categories_div").addClass("hide");
        }
        else{
            $(this).find(".success_message").text(sender_status.status);
        }
        return false;
    });//end of form submit

    //Display list of categories
    var categories_table =$("#categories_table");
    var get_categories_url = api_url+"dashboard/querycategories/";
    var get_categories_ajax_data = {"op":"get","user_id":admin_id};
    var string_strip ='';
    var get_categories_data = call_ajax("POST",get_categories_url, get_categories_ajax_data);
    if(get_categories_data.length > 0){
        header_row = "<tr>";
        header_row += "<td>Category Name</td><td>Sub Categories</td>" +
            "<td>Update</td><td>Delete</td>";
        header_row += "</tr>";
        get_categories_data.forEach(function (category_data) {
            string_strip='';
            var display_category_name = category_data.category_name;
            if (category_data.category_name.length > 30)
                display_category_name = category_data.category_name.substring(0,30)+"...";
            category_data.subcategories.forEach(function (sub_categories_data) {
                string_strip += sub_categories_data.name + ", ";
            });
            string_strip = string_strip.replace(/,([^,]*)$/,''+'$1');
            if (string_strip.length > 30)
                display_string_strip = string_strip.substring(0,30)+"...";
            else
                {
                    string_strip.slice(0,-2);
                    display_string_strip = string_strip;
                }
            body_row = "<tr><td class='category_name' title='"+replaceAll(category_data.category_name,' ','')+"'" +
                " id='c_"+category_data.category_id+"'>"+display_category_name+"</td>" +
                "<td class='subcategories_name' title='"+string_strip+"'>";
            body_row += display_string_strip;
            body_row += "</td><td>" +
                "<a href='javascript:void' class='update_icon'><i class='fa fa-pencil'></i></a>" +
                "</td><td>" +
                "<a href='javascript:void' class='delete_icon'><i class='fa fa-close'></i></a>" +
                "</td></tr>";
            categories_table.find("tbody").append(body_row);
        });
        categories_table.find("thead").append(header_row);
    }
    else{
        categories_table.siblings("h3").text("");
        categories_table.find("thead").append("<h4>No Categories Exist</h4>");
    }
    //end of list display
    $(".update_icon").on("click",function (event) {

        event.preventDefault();
        //category id = $(this).parent().parent().find(".category_name").attr("id").split("_")[1]
        //category id = $(this).parent().parent().find(".category_name").text();
        var categories_table =$("#categories_table");
        var get_categories_url = api_url+"dashboard/querycategories/";
        var get_categories_ajax_data = {"op":"get","user_id":admin_id};
        var string_strip ='';
        var get_categories_data = call_ajax("POST",get_categories_url, get_categories_ajax_data);

        var category_id = $(this).parent().parent().find(".category_name").attr("id").split("_")[1];
//        var category_name = $(this).parent().parent().find(".category_name").attr("title");
        var index = get_categories_data.findIndex(function(x) { return x.category_id == category_id });
        var category_name = get_categories_data[index].category_name;
        $(".categories_div h3").text("Update Categories");
        var display_category_name = category_name;
        if(category_name.length>30)
        {
          display_category_name=category_name.substring(0,30)+"...";
        }
        $("input[name='category_name']").val(category_name).attr("val",category_id);
        var sub_categories_rows ='';
        $.each(get_categories_data[index]['subcategories'],function (subcategory_index, sub_category) {

//            sub_cat_to_be_displayed +=sub_category.name+","
            sub_categories_rows += '<div><input type="text" required name="sub_categories"  ' +
                ' placeholder="Enter Subcategory Name" class="form-control subcategories" ' +
                ' value="'+sub_category.name+'" val="'+sub_category.id+'"/>';
            if(subcategory_index != 0) sub_categories_rows+='<a href="#" class="remove_field"><img class="form-control" src="/static/img/remove-icon.png"></a>';
            sub_categories_rows+='</div>';
        });
//        if (sub_cat_to_be_displayed.length>0)
//            sub_cat_to_be_displayed= sub_cat_to_be_displayed.substring(0,sub_cat_to_be_displayed.length-1)+".";
        $(".categories_btn").val("Update");
        $(sub_categories_wrapper).html(sub_categories_rows);
        $(".categories_div").removeClass("hide");
    });
    $(".delete_icon").on("click",function (event) {
        event.preventDefault();
        // $(".categories_div form").find("input[type=text], textarea").val("").hide();
        var category_id = $(this).parent().parent().find(".category_name").attr("id").split("_")[1];
        var category_name = $(this).parent().parent().find(".category_name").text();
        if(confirm("Are you sure to delete this Category ! Category Name :"+category_name+" ?") == true){
            var ajax_data = {"op":"delete","category_id":category_id};
            var ajax_url = api_url + "dashboard/querycategories/";
            var get_deleted_status = ajax_call_with_login("POST", ajax_url, ajax_data);
            if(get_deleted_status.status == "deleted")
            {
               $(this).parent().parent().hide();
            }
        }
        else{
            return false;
        }
        // $(".categories_div").addClass("hide");
    });
    //end of Categories Module

    /**
    * -------------------------------------
    *  Pass code Module
    * -------------------------------------
    */
    var selected_media_id="";
    var passcode_selecter=$("#passcode");
    var passcode_submit=$("#passcode_submit");
    var project_dd =$("#project_dd");
    var media_dd = $("#media_dd");
    var api_host =$("#api_url").val();
    var user_id = $("#user_id").val();
    var project_url = api_host + "dashboard/selectuserproject/";
    var json_data = {"user_id":user_id};
    var get_project_list = call_ajax("POST",project_url,json_data);
    if(get_project_list.length == 0){
        $(".project_list.dropdown_btn").html("No project list for this user");    
    }
    else
    {
        var isfirstProject = true;
        get_project_list.forEach(function (data) {
            var selected = "";
            if(project_id == "" && isfirstProject || project_id==data.project_id)
                selected = "selected='selected' "

            project_list += "<option id='project_"+data.project_id+"' title='"+data.name+"' "+selected+">"+data.name+"</option>";
            isfirstProject = false;
        });
        project_dd.append(project_list).selectpicker('refresh');
    }

    project_dd.on('change',function () {
         // pick out project id 
        var project_id = $(this).children(":selected").attr("id").split("_")[1];
        // selected index of the project in the dropdown
        var index = get_project_list.findIndex(function(x) {return x.project_id == project_id});


        var selected_project = get_project_list.filter(x => x.project_id == project_id);
        var media_in_project = selected_project[0].media;
        var media_list_filtered = media_in_project.filter(x => x.project_id == project_id);
        var media_list = "";
        // var user_list = "<option id='user_all' selected>ALL</option>";
        var user_list = "";

        var unique_users_array = new Set();
        var default_media_flag = 0;
        if(selected_project.length > 0 ){

            media_list_filtered.forEach(function (data) {
                if(data.name!=null) {
                    var selected_media = "";
                    if (default_media_flag == 0 && data.testers.length != 0) {
                        selected_media = " selected"; //space is required
                        data.testers.forEach(function (rec) {
                            if(rec.name!=null) {
                                if (!unique_users_array.has(rec.id)) {
                                    user_list += "<option id='usertester_" + rec.id + "' value='usertester_" + rec.id + "'  title='" + rec.name + "' >" + rec.name + "</option>";
                                    unique_users_array.add(rec.id);
                                }
                            }
                        });
                    }

                    var disable = "";
                    if(data.testers.length == 0)
                        disable = " style='background: #dd7c44; color: #fff' ";
                    else
                        default_media_flag = 1;

                    media_list += "<option  id='" + data.media_id + "' value='" + data.media_id + "'  title='" + data.name + "'" +selected_media + disable  + ">" + data.name + "</option>";


                }
            });
            media_dd.html("");
            media_dd.append(media_list).selectpicker('refresh');
            (media_list.length == 0) ? passcode_selecter.text("No Media Exists") : media_dd.trigger("change");
        }else{
            media_dd.html("");
            media_dd.append(media_list).selectpicker('refresh');
        }
    });

    // Media dropdown change
    media_dd.on('change',function(){
        //check the media already has any passcode or not
        selected_media_id = $('#media_dd').find("option:selected");
        if(selected_media_id == undefined){
            passcode_selecter.text("No Passcode");
            return;
        }
        var get_passcode_url = api_url+"dashboard/get_passcode/"+selected_media_id.val();
        var get_passcode = call_ajax("GET",get_passcode_url,"");
        if(get_passcode.status == "No Passcode Generated for test, please create one")
        {
            passcode_selecter.text(Math.random().toString(36).substr(7));
            // passcode_submit.show();
        }
        else
        {
            // passcode_submit.hide();
            passcode_selecter.text(get_passcode.status);   
        }

    });
    //generate passcode
    passcode_submit.on('click',function(){
        selected_media_id = $('#media_dd').find("option:selected").val();
        var passcode_url = api_url+"dashboard/generate_passcode/";
        var generate_data = { 'media_id':selected_media_id};
        var get_passcode = call_ajax("POST",passcode_url,generate_data);
        if(get_passcode.status == "success" ){
            $(".passcode_success").text("New Passcode Generated").fadeIn(3000).fadeOut(3000);
            // $(this).hide();
            if(get_passcode.passcode != null)
            passcode_selecter.text(get_passcode.passcode);
        }
        // debugger;
    });

    $("#create_pseudo_tester").on("click",function(){
        var tester_prefix = $("#tester_prefix");
        var tester_password = $("#tester_password");
        var no_of_testers = $("#no_of_testers");
        var start_from = $("#start_from");
        if (tester_prefix.val() == "")
        {
            $("#pseudotester_success").text("Enter Tester Prefix").fadeIn(3000).fadeOut(3000);
        }
        else if(tester_password.val() == ""){
            $("#pseudotester_success").text("Enter Tester Password").fadeIn(3000).fadeOut(3000);
        }
        else if(tester_password.val().length < 6){
            $("#pseudotester_success").text("Tester Password must be of 6 letters").fadeIn(3000).fadeOut(3000);
        }
        else if( tester_prefix.val() != "" && tester_password.val() != "" && tester_password.val().length >= 6)
        {
            var generate_data = { 'tester_prefix':tester_prefix.val(),'number':no_of_testers.val(),
                            'password':tester_password.val(),'admin_id':admin_id,'start':start_from.val()};
            var pseudotester_url = api_url+"dashboard/createpseudotester/";
            var create_pseudotester = call_ajax("POST",pseudotester_url,generate_data);
            if(create_pseudotester.status == "Created" ){
                $("#pseudotester_success").text("New pseudotesters Generated").fadeIn(3000).fadeOut(3000);
                tester_prefix.val("");
                tester_password.val("");
                no_of_testers.val(1);
                start_from.val(1);
                // if(create_pseudotester.passcode != null)
                // passcode_selecter.text(get_passcode.passcode);
            }
            return;
        }
    });
});
