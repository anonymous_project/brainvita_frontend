from __future__ import unicode_literals
from django.views import generic
from django.conf import settings
# settings.LOGIN_URL='/tester/login/'
# settings.LOGIN_REDIRECT_URL='/tester/home_view/'
# User = get_user_model()


class SignUpView(generic.TemplateView):
    template_name = "delegate/signup.html"
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        kwargs['api_url'] = settings.API_ENDPOINT
        return super(SignUpView, self).get(self, *args, **kwargs)
