from django.conf.urls import url
from delegate.views import *
urlpatterns = [
    url(r'^signup/(?P<uuid>.+)$', SignUpView.as_view(), name='signup'),
    # url(r'^home_view/$', HomeView.as_view(), name='home_view'),
    # url(r'^dashboard_view/$', DashboardView.as_view(), name='dashboard_view'),
    # url(r'^login/$', LoginView.as_view(), name="login"),
    # url(r'^logout/$', LogoutView.as_view(), name='logout'),
    # url(r'^password-change/$', PasswordChangeView.as_view(), name='password-change'),
    # url(r'^password-reset/$', PasswordResetView.as_view(), name='password-reset'),
    # url(r'^password-reset-done/$', PasswordResetDoneView.as_view(), name='password-reset-done'),
    # url(r'^password-reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$$',
    #     PasswordResetConfirmView.as_view(),
    #     name='password-reset-confirm'),
]

