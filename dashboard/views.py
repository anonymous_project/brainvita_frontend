from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
import requests,subprocess
from rest_framework.views import APIView
from rest_framework.response import Response
import re,boto3,threading
from dashboard import forms
# USERNAME_PASSWORD = "admin:admin@123"

"""
Getting Started View to create a new application
@input application_name,category,platform,time_zone
@output project_token
"""
# User =get_user_model()

def async(function):
    def async_decorator(*args, **kwargs):
        try:
            t = threading.Thread(target=function,args=args,kwargs = kwargs)
            t.setDaemon(False)
            t.start()
        except:
            return {"status": "Unsuccesfull"}
    return async_decorator


class VisionView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/vision_view.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['api_host'] = settings.API_HOSTNAME
        return super(VisionView, self).get(request, *args, **kwargs)


class UploadFile(APIView):
    def post(self,request,media_id=''):
        if request.method != 'POST':
            return Response({"response":'Only POST requests are allowed'})
        file = request.FILES['filetoupload']
        file.name = filter(lambda x: str.isalnum(x) or "." == x or "-" == x or  "_" == x, file.name.encode())
        with open('static/media/%s' % str(media_id)+"_"+file.name,'wb+') as dest:
            for chunk in file.chunks():
                dest.write(chunk)
        print file.name.replace('(','\(').replace(')',"\)")
        png = "".join(file.name.split(".")[:-1])
        subprocess.call("ffmpeg -i static/media/"+str(media_id)+"_"+re.escape(file.name)+" -vcodec  png -ss 1 -vframes 1 -an -f  rawvideo static/media/"+str(media_id)+"_"+png+".png",shell=True)
        return Response({"response":'File uploaded'})
            



class HomeView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/home_view.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['api_host'] = settings.API_HOSTNAME
        return super(HomeView, self).get(request, *args, **kwargs)


class DashboardView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/dashboard_view.html"

    def get(self, request, project_id='', media_id='', *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['host_url'] = settings.API_HOSTNAME
        kwargs['project_id'] = project_id
        kwargs['media_id'] = media_id
        return super(DashboardView, self).get(request, *args, **kwargs)


class CreateTestView(LoginRequiredMixin, APIView, generic.TemplateView):
    template_name = "dashboard/create_test_view.html"
    http_method_names = ['get', 'post']
    form_class = forms.CreateTestForm

    def get(self, request, project_id='', media_id='', *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['host_url'] = settings.API_HOSTNAME
        kwargs['project_id'] = project_id
        kwargs['media_id'] = media_id
        #form=self.form_class,
        return super(CreateTestView, self).get(request, *args, **kwargs)

    def post(self, request, project_id='', media_id=''):

        return redirect("dashboard:project_overview")


class TestDetails(LoginRequiredMixin, APIView, generic.TemplateView):
    template_name = "dashboard/test_details.html"
    http_method_names = ['get', 'post']
    def get(self, request, project_id='', media_id='', *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['host_url'] = settings.API_HOSTNAME
        kwargs['project_id'] = project_id
        kwargs['media_id'] = media_id
        return super(TestDetails, self).get(request, *args, **kwargs)

    def post(self, request, project_id='', media_id=''):
        return redirect("dashboard:project_overview")



class ProjectView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/project_view.html"

    def get(self, request, project_id='',*args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['host_url'] = settings.API_HOSTNAME
        kwargs['project_id'] = project_id
        return super(ProjectView, self).get(request, *args, **kwargs)


class MediaView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/media_overview.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['host_url'] = settings.API_HOSTNAME
        return super(MediaView, self).get(request, *args, **kwargs)

    def post(self):
        return redirect("dashboard")


class AddMediaView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/add_media.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        return super(AddMediaView, self).get(request, *args, **kwargs)

class MediaOverview(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/media_overview_list.html"

    def get(self, request, *args, **kwargs):
        kwargs["project_id"] = kwargs["project_id"]
        kwargs['user'] = self.request.user
        kwargs['host_url'] = settings.API_HOSTNAME
        kwargs['api_url'] = settings.API_ENDPOINT
        return super(MediaOverview, self).get(request, *args, **kwargs)


class MediaReportsView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/media_overview_report.html"

    def get(self, request, *args, **kwargs):
        kwargs["project_id"] = kwargs["project_id"]
        kwargs["media_id"] = kwargs["media_id"]
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        return super(MediaReportsView, self).get(request, *args, **kwargs)


class ReportsView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/reports_view.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        return super(ReportsView, self).get(request, *args, **kwargs)


class CompareReportsView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/compare_reports_view.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        return super(CompareReportsView, self).get(request, *args, **kwargs)


class SettingsView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/settings.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        return super(SettingsView, self).get(request, *args, **kwargs)

class ManageProjectView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/manage_projects.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['host_url'] = settings.API_HOSTNAME
        return super(ManageProjectView, self).get(request, *args, **kwargs)

class AddProjectView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/add_project.html"

    def get(self, request,project_id='', *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['host_url'] = settings.API_HOSTNAME
        kwargs['project_id'] = project_id
        return super(AddProjectView, self).get(request, *args, **kwargs)

class AddTestView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/add_test.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['host_url'] = settings.API_HOSTNAME
        kwargs['project_id'] = request.GET.get('project_id')
        return super(AddTestView, self).get(request, *args, **kwargs)


class EditTestView(LoginRequiredMixin, APIView, generic.TemplateView):
    template_name = "dashboard/edit_test.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['host_url'] = settings.API_HOSTNAME
        kwargs['media_id'] = request.GET.get('media_id')
        kwargs['project_id'] = request.GET.get('project_id')
        kwargs['activity_type'] = request.GET.get('activity_type')
        return super(EditTestView, self).get(request, *args, **kwargs)


class TrackTestView(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard/test_details.html"

    def get(self, request, *args, **kwargs):
        kwargs['user'] = self.request.user
        kwargs['api_url'] = settings.API_ENDPOINT
        kwargs['host_url'] = settings.API_HOSTNAME
        kwargs["project_id"] = kwargs["project_id"]
        kwargs["media_id"] = kwargs["media_id"]
        return super(TrackTestView, self).get(request, *args, **kwargs)


class S3FileUploadAPI(APIView):

    def __init__(self):
        self.s3 = boto3.resource('s3')
        self.client = boto3.client('s3')

    def common_read(self,bucket,prefix,filename):
        try :
            bucket = self.s3.Bucket(bucket)
            prefix = prefix.strip().rstrip('/')
            object = self.bucket.Object(prefix + '/' + filename.strip())
            return object.get()['Body'].read()
        except:
            return ""

    def common_write(self,bucket,prefix,filename,body):
        self.s3 = boto3.resource('s3')
        self.client = boto3.client('s3')
        prefix = prefix.strip().rstrip('/')
        filename = filename.strip()
        #print bucket + '/'+prefix + '/' + filename
        object = self.s3.Object(bucket, prefix+'/'+filename)
        try:
            self.client.delete_object(Bucket=bucket, Key=prefix+'/'+filename)
        except:
        #   return {'status':'error'}
           pass
        object.put(Body=body)
        return {'status':'success'}

    # @async
    def run_async(self,bucket,prefix,filename,body):
        status = self.common_write(bucket, prefix, filename, body)
        return status

    def post(self,request):
        #bucket = request.data['bucket']
        bucket = 'neurosensum'
        prefix = request.data['prefix']
        filename = request.data['filename']
        body = request.FILES['file']
        self.run_async(bucket,prefix,filename,body)
        url = 'https://s3.amazonaws.com/'+bucket.strip('/')+'/'+prefix.strip('/')+'/'+filename
        return Response({'status':'Uploading in background','url':url})


