from __future__ import unicode_literals
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from authtools import forms as authtoolsforms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, HTML, Field
# from dashboard.models import CreateTest
from django.core.urlresolvers import reverse

# class FileField(Field):
#     template = 'bootstrap/layout/file_field.html'


class CreateTestForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(CreateTestForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields['email'].widget.input_type = "text"  # ugly hack

        self.helper.layout = Layout(
            # FileField('main_image',placeholder="Enter Email", autofocus=""),
            # Field('test_image', placeholder="Enter Email", autofocus=""),
            Field('email', placeholder="Enter email", autofocus="", css_class="form-control"),

            Submit('launch', 'Launch',css_class="btn btn-lg btn-primary btn-block"),
        )
    #
    # class Meta:
    #     model = CreateTest
