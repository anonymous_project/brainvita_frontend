from django.conf.urls import url

from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^home/$', views.HomeView.as_view(), name='home_view'),
    url(r'^vision/$', views.VisionView.as_view(), name='vision_view'),
    url(r'^dashboard/$', views.DashboardView.as_view(), name='dashboard_view'),
    url(r'^dashboard/(?P<project_id>[0-9]+)/(?P<media_id>[0-9]+)$', views.DashboardView.as_view(), name='dashboard_view'),
    url(r'^create_test_view/$', views.CreateTestView.as_view(), name='create_test_view'),
    url(r'^create_test_view/(?P<project_id>[0-9]+)/(?P<media_id>[0-9]+)$', views.CreateTestView.as_view(), name='create_test_view'),
    url(r'^project_overview/$', views.ProjectView.as_view(), name='project_overview'),
    url(r'^project_overview/(?P<project_id>[0-9]+)$', views.ProjectView.as_view(), name='test_view'),
    url(r'^add_project/$', views.AddProjectView.as_view(), name='add_project'),
    url(r'^add_project/(?P<project_id>[0-9]+)$', views.AddProjectView.as_view(), name='edit_project'),
    url(r'^add_test/$', views.AddTestView.as_view(), name='add_test'),
    url(r'^edit_test/$', views.EditTestView.as_view(), name='edit_test'),
    url(r'^add_media/(?P<project_id>[0-9]+)$', views.AddMediaView.as_view(), name='add_media'),
    url(r'^manage_project/$', views.ManageProjectView.as_view(), name='manage_project'),
    url(r'^media_overview/$', views.MediaView.as_view(), name='media_overview'),
    url(r'^media_overview_list/(?P<project_id>[0-9]+)$', views.MediaOverview.as_view(), name='media_overview_list'),
    url(r'^media_overview_report/(?P<project_id>[0-9]+)/(?P<media_id>[0-9]+)$', views.MediaReportsView.as_view(), name='media_overview_report'),
    url(r'^view_reports/$', views.ReportsView.as_view(), name='view_reports'),
    url(r'^compare_reports/$', views.CompareReportsView.as_view(), name='compare_reports'),
    url(r'^track_test/(?P<project_id>[0-9]+)/(?P<media_id>[0-9]+)$', views.TestDetails.as_view(), name='track_test'),
    url(r'^uploadfile/(?P<media_id>[0-9]+)$', views.UploadFile.as_view(), name='uploadfile'),
    url(r'^s3uploadvideo/', views.S3FileUploadAPI.as_view(), name='s3uploadvideo'),
    url(r'^settings/$', views.SettingsView.as_view(), name='settings'),

]