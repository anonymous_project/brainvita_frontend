#!/bin/bash
# Starts the Gunicorn server
set -e

# Activate the virtualenv for this project
source /home/ubuntu/.virtualenvs/brainvita_frontend/bin/activate

# Start gunicorn going
exec gunicorn brainvita_frontend.wsgi:application --pythonpath . -c /home/ubuntu/brainvita_frontend/gunicorn.conf.py




