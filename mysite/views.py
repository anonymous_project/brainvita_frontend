from django.shortcuts import render
from django.views import generic
from accounts.utils import send_email
from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect

# Create your views here.

class HomePage(generic.TemplateView):
    template_name = "lpage/index.html"

    def get(self, request, *args, **kwargs):
        kwargs['api_url'] = settings.API_ENDPOINT
        return super(HomePage,self).get(self,request,*args, **kwargs)

    def send_contact_mail(self, request):
        """
        Sending the contact email to the admin user
        """
        if request.method == 'POST':
            email = request.POST.get('email')
            message = request.POST.get('message')
            name = request.POST.get('subject')
            data = dict()
            data['from'] = email
            data['subject'] = 'Contact Query'
            data['to'] = settings.CONTACT_TO_EMAIL
            data['body'] = "Name : " + name  +"\n" + "E-mail : " + email + "\n" + "Message : " +  message
            send_email(data)
            messages.success(request,"Your request has been submitted successfully")
        return redirect('/#contactus')
