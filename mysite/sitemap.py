from django.contrib import sitemaps
from django.core.urlresolvers import reverse

class StaticViewSitemap(sitemaps.Sitemap):
    priority = 1
    changefreq = 'weekly'

    def items(self):
       return ['home','technology','media_research','brand_research','product_research','termscondition','privacy']

    def location(self, item):
        return reverse(item)




