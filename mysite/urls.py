from django.conf.urls import include, url
from mysite.views import *
from django.contrib.sitemaps.views import sitemap
from .sitemap import StaticViewSitemap
from django.views.generic import TemplateView

sitemaps = {
    'static': StaticViewSitemap,
}

urlpatterns=[
    url(r'^$',HomePage.as_view(), name='home'),
    url(r'^sendcontactmail/$', HomePage().send_contact_mail, name='sendcontactmail'),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
            name='django.contrib.sitemaps.views.sitemap'),
    url(r'^robots.txt$', TemplateView.as_view(template_name="robots.txt", content_type="text/plain"), name="robots_file"),
]


